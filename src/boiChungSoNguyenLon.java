import java.math.BigInteger;
import java.util.Scanner;

public class boiChungSoNguyenLon {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.valueOf(sc.nextLine());
        while(t-->0) {
            BigInteger a = new BigInteger(sc.nextLine());
            BigInteger b = new BigInteger(sc.nextLine());
            System.out.println(a.multiply(b).divide(a.gcd(b)));
        }
    }
}
