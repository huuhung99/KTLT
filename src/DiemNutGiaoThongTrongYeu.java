import java.util.ArrayList;
import java.util.Scanner;

public class DiemNutGiaoThongTrongYeu {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n= sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            int dem=0;
            for (int i=1;i<=n;i++){
                if(chuaxet[i]){
                    dfs(i);
                    dem++;
                }
            }
            ArrayList<Integer> kq=new ArrayList<>();
            for (int i=1;i<=n;i++){
                for(int s=0;s<=n;s++) chuaxet[s]=true;
                chuaxet[i]=false;
                int tmp=0;
                for (int k=1;k<=n;k++){
                    if(chuaxet[k]){
                        dfs(k);
                        tmp++;
                    }
                }
                if(tmp!=dem) kq.add(i);
            }
            System.out.println(kq.size());
            for(Integer integer:kq)
                System.out.print(integer+" ");
            System.out.println();
        }
    }

    private static void dfs(int i) {
        chuaxet[i]=false;
        for(Integer k:lists[i]){
            if (chuaxet[k]){
                chuaxet[k]=false;
                dfs(k);
            }
        }
    }
}
