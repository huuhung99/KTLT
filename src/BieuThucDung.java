import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Stack;

public class BieuThucDung {

  public static void main(String[] args) throws FileNotFoundException {
    Scanner sc=new Scanner(System.in);
    sc=new Scanner(new File("C:\\Users\\Admin\\Desktop\\SPOJ\\src\\input.txt"));
    int t=Integer.parseInt(sc.nextLine());
    Stack<Character> stack=new Stack<>();
    int count=0;
    while (t-->0){
      String s=sc.nextLine();
      for(int i=0;i<s.length();i++){
        if(stack.empty()) stack.push(s.charAt(i));
        else{
          char pop = stack.pop();
          if(s.charAt(i)==pop) count++;
          else if(s.charAt(i)=='['&&pop==']') count++;
        }
      }
      System.out.println(count);
      count=0;
    }
  }
}
