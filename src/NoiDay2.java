import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class NoiDay2 {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while(t-->0){
      int n=sc.nextInt();
      List<Long> list = new ArrayList<Long>() {
        public boolean add(Long mt) {
          int index = Collections.binarySearch(this, mt);
          if (index < 0) index = ~index;
          super.add(index, mt);
          return true;
        }
      };
      for (int i=0;i<n;i++){
        list.add(sc.nextLong());
      }
      long result=0;
      while(list.size()>1){
        long tmp=list.get(0)+list.get(1);
        result+=tmp;
        list.add(tmp);
        list.remove(0);
        list.remove(0);
      }
//      result+=list.get(0);
      System.out.println(result%1000000007);
    }
  }
}
