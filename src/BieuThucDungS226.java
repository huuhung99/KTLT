import java.util.Scanner;
import java.util.Stack;

public class BieuThucDungS226 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.valueOf(sc.nextLine());
        Stack<Character> stack=new Stack<>();
        while(t-->0){
            String s=sc.nextLine();
            int count=0;
            int max=0;
            boolean check=true;
            for(int i=0;i<s.length();i++){
                if(s.charAt(i)=='('){
                    stack.push('(');
                    count=stack.size();
                    max=Math.max(count,max);
                }
                else{
                    if(s.charAt(i)==')'){
                        if(stack.empty()){
                            System.out.println(-1);
                            check=false;
                            break;
                        }
                        stack.pop();
                    }
                }
            }
            if(check==true){
                if(stack.empty()) System.out.println(max);
                else System.out.println(-1);
            }
        }
    }
}
