import java.util.Scanner;

public class KhaiBaoLopSinhVien {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String name=sc.nextLine();
        String maSv=sc.nextLine();
        String ngaySinh=sc.nextLine();
        Float gpa=Float.valueOf(sc.nextLine());
        String[] split = ngaySinh.split("/");
        if(split[0].length()==1) split[0]=0+split[0];
        if(split[1].length()==1) split[1]=0+split[1];
        String dateOfBirth="";
        for(String s:split)
            dateOfBirth+=s+"/";
        System.out.format("B20DCCN001 %s %s %s\b %.2f",name,maSv,dateOfBirth,gpa);
    }
}
