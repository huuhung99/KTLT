import java.util.Scanner;

public class SapXepChen {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
//        int t
//                =Integer.valueOf(sc.nextLine())
//                ;
//        while(t-->0){
        int n=sc.nextInt();
        int[] arr=new int[105];
        int[] a=new int[n];
        for(int i=0;i<105;i++)
            arr[i]=0;
        for(int i=0;i<n;i++)
            a[i]=sc.nextInt();
        for(int i=0;i<n;i++){
            arr[a[i]]++;
            printfArray(arr,i);
        }
//        }
    }

    private static boolean isSorted(int[] a) {
        for(int i=0;i<a.length-1;i++)
            if(a[i]>a[i+1]) return false;
        return true;
    }

    private static void printfArray(int[] a,int buoc) {
        System.out.format("Buoc %d: ",buoc);
        for(int i=0;i<105;i++)
            for(int j=0;j<a[i];j++)
                System.out.format("%d ",i);
        System.out.println();
    }
}
