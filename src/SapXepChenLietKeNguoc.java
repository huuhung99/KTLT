import java.util.Scanner;

public class SapXepChenLietKeNguoc {
    private static long m=1000000007;
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
//        int t
//                =sc.nextInt()
//                ;
//        while(t-->0){
        int n=sc.nextInt();
        int[] a=new int[n];
        for(int i=0;i<n;i++)
            a[i]=sc.nextInt();
        sort(a,n,"");
//        }
    }

    private static void sort(int arr[],int n,String s) {
        s=printf(arr,0,"",0)+s;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
            s=printf(arr,i,"",i)+s;
        }
        System.out.print(s);
    }

    private static String printf(int[] arr, int i,String result,int buoc) {
        result+=String.format("Buoc %d: ",buoc);
        for(int j=0;j<=i;j++)
            result+=String.format("%d ",arr[j]);
        return result+="\n";
    }
}
