import java.util.*;

public class SoLocPhatS215 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt();
            Queue<String> queue=new ArrayDeque<>();
            List<String> list=new ArrayList<>();
            queue.add("6");
            queue.add("8");
            while(queue.peek().length()<=n){
                String poll = queue.poll();
                list.add(poll);
                queue.add(poll+"6");
                queue.add(poll+"8");
            }
            System.out.println(list.size());
            for(int i= list.size()-1;i>=0;i--)
                System.out.print(list.get(i)+" ");
            System.out.println();
        }
    }
}
