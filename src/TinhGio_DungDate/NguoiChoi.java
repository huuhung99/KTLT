package TinhGio_DungDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NguoiChoi implements Comparable<NguoiChoi>{
    private String ma,ten;
    private Date vao,ra;
    private long giayChoi;
    private long gioChoi;
    private long phutChoi;

    public NguoiChoi(String ma, String ten, String vao, String ra) throws ParseException {
        this.ma = ma;
        this.ten = ten;
        this.vao = new SimpleDateFormat("hh:mm").parse(vao);
        this.ra = new SimpleDateFormat("hh:mm").parse(ra);
        if(!this.ra.after(this.vao)){
            this.ra=new Date(this.ra.getTime()+1000*60*60*24);
        }
        this.giayChoi=(this.ra.getTime()-this.vao.getTime())/1000;
        // giay=60*60*gio+60*phut
        this.phutChoi=(this.giayChoi/60)%60;
        this.gioChoi=this.giayChoi/(60*60);
    }

    @Override
    public String toString() {
        return String.format("%s %s %d gio %d phut",ma,ten,gioChoi,phutChoi);
    }

    @Override
    public int compareTo(NguoiChoi o) {
        return giayChoi<o.giayChoi?1:-1;
    }
}
