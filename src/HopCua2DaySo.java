import java.util.*;
import java.util.stream.Collectors;

public class HopCua2DaySo {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt(),b=sc.nextInt();
        Set<Integer> set=new HashSet<>();
        for(int i=0;i<a+b;i++)
            set.add(sc.nextInt());
        List<Integer> collect = set.stream().sorted().collect(Collectors.toList());
        for(int i=0;i<collect.size();i++)
            System.out.print(collect.get(i)+" ");
    }
}
