import java.util.Scanner;

public class DaoTu {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t=Integer.valueOf(sc.nextLine());
        while(t-->0){
            String[] split = sc.nextLine().split("\\W+");
            for(int i=0;i< split.length;i++){
                if(split[i].isEmpty()||split[i].equals("")) continue;
                for(int j=split[i].length()-1;j>=0;j--){
                    System.out.format("%c",split[i].charAt(j));
                }
                System.out.print(" ");
            }
            System.out.println();
        }
    }

}
