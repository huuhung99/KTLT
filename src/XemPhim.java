import java.util.Scanner;

public class XemPhim {
    static int c[][];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt(),v= sc.nextInt();
        int a[]=new int[v+1],b[]=new int[v+1];
        for(int i=1;i<=v;i++) a[i]=sc.nextInt();
        for(int i=1;i<=v;i++) b[i]=1;
        System.out.println(qhd(a,b,n,v));
    }

    private static int qhd(int[] a, int[] b, int n, int v) {
        c=new int[v+1][n+1];
        for(int i=1;i<v;i++){
            for(int j=1;j<n;j++){
                c[i][j]=c[i-1][j];
                if(j>=a[i]) Math.max(c[i-1][j],c[i-1][j-a[i]] + a[i]);
            }
        }
        return c[v][n];
    }
}
