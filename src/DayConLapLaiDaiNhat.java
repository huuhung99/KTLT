import java.util.Scanner;

public class DayConLapLaiDaiNhat {
    static int c[][];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            int n=sc.nextInt();
            String s=sc.next();
            c=new int[n+1][n+1];
            for(int i=1;i<=n;i++){
                for(int j=1;j<=n;j++){
                    if(i!=j&& s.charAt(i-1)==s.charAt(j-1)) c[i][j]=c[i-1][j-1]+1;
                    else c[i][j]=Math.max(c[i-1][j],c[i][j-1]);
                }
            }
            System.out.println(c[n][n]);
        }
    }
}
