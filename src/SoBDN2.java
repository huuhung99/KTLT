import java.util.*;

public class SoBDN2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t= sc.nextInt();
        Queue<String> queue=new ArrayDeque<>();
        Set<Integer> set=new HashSet<>();
        String s;
        while (t-->0){
            int n=sc.nextInt();
            queue.add("1");
            while (!queue.isEmpty()){
                s=queue.remove();
                int i = chiaHet(s, n);
                if(i==0){
                    System.out.println(s);
                    queue.clear();
                    set.clear();
                    break;
                }
                else if(!set.contains(i)){
                    set.add(i);
                    queue.add(s+"0");
                    queue.add(s+"1");
                }
            }
        }
    }
    private static int chiaHet(String s,int n){
        int sum=0;
        for(int i=0;i<s.length();i++){
            sum=sum*10+s.charAt(i)-'0';
            sum%=n;
        }
        return sum;
    }
}
