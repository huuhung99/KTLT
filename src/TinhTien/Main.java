package TinhTien;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<HoaDon> list=new ArrayList<>();
        while(t-->0){
            list.add(new HoaDon(sc.nextLine(),sc.nextLine(),Long.parseLong(sc.nextLine())
                    ,Long.parseLong(sc.nextLine()),Long.parseLong(sc.nextLine())));
        }
        Collections.sort(list);
        for(HoaDon hoaDon:list)
            System.out.println(hoaDon);
    }
}
