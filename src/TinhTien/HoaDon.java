package TinhTien;

public class HoaDon implements Comparable<HoaDon>{
    private String ma,ten;
    private long soLuong,donGia,chietKhau,soTien;

    public HoaDon(String ma, String ten, long soLuong, long donGia, long chietKhau) {
        this.ma = ma;
        this.ten = ten;
        this.soLuong = soLuong;
        this.donGia = donGia;
        this.chietKhau = chietKhau;
        this.soTien=this.soLuong*this.donGia-this.chietKhau;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %d %d %d",ma,ten,soLuong,donGia,chietKhau,soTien);
    }

    @Override
    public int compareTo(HoaDon o) {
        return soTien<o.soTien?1:-1;
    }
}
