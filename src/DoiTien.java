import java.util.Scanner;

public class DoiTien {
    static int a[],k;
    static int n;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        n=sc.nextInt();k=sc.nextInt();
        a=new int[n];
        for(int i=0;i<n;i++) a[i]=sc.nextInt();
        System.out.println(minCoins(a,n,k));
    }
    static int minCoins(int coins[], int m, int V)
    {
        if (V == 0) return 0;

        // Initialize result
        int res = Integer.MAX_VALUE;

        // Try every coin that has smaller value than V
        for (int i=0; i<m; i++)
        {
            if (coins[i] <= V)
            {
                int sub_res = minCoins(coins, m, V-coins[i]);

                // Check for INT_MAX to avoid overflow and see if
                // result can minimized
                if (sub_res != Integer.MAX_VALUE && sub_res + 1 < res)
                    res = sub_res + 1;
            }
        }
        return res;
    }
}
