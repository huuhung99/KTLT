import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class GoBanPhim {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    char[] s = sc.nextLine().toCharArray();
    Stack<Character> stack=new Stack<>();
    Stack<Character> tmp=new Stack<>();
    for(int i=0;i<s.length;i++){
      if(s[i]=='<'){
        if(!stack.empty())tmp.push(stack.pop());
      }
      else if(s[i]=='>'){
        if(!tmp.empty())stack.push(tmp.pop());
      }
      else if(s[i]=='-'){
        if(!stack.empty())stack.pop();
      }
      else{
        stack.push(s[i]);
      }
    }
    while(!tmp.empty()){
      stack.push(tmp.pop());
    }
    List<Character> list=new ArrayList<>(stack);
    for (Character o : list) {
      System.out.print(o);
    }
//    Object[] objects = stack.stream().toArray();
//    for (Object o : objects) {
//      System.out.print(o);
//    }
  }
}
