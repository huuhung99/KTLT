import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.PriorityBlockingQueue;

public class NoiDay {
  static int m= (int) (1e9+7);
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    PriorityQueue<Long> pq = new PriorityQueue<>();
    while (t-->0){
      int n=sc.nextInt();
      for (int i = 0; i < n; i++) {
        pq.add(sc.nextLong());
      }
      Queue<Long> queue= new PriorityBlockingQueue<>();
      long result = 0;
      while (pq.size() > 1) {
        long so1 = pq.remove();
        long so2 = pq.remove();
        long sum=(so1+so2)%m;
        result = (result+ sum)%m;
        pq.add(sum);
      }
      System.out.println(result);
      pq.clear();
    }
  }
}
