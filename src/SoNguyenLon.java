import java.util.Scanner;

public class SoNguyenLon {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            String x=sc.next();String y=sc.next();
            int a[][]=new int[x.length()+1][y.length()+1];
            for(int i=1;i<x.length()+1;i++){
                for(int j=1;j<y.length()+1;j++){
                    if(x.charAt(i-1)==y.charAt(j-1)){
//                        a[i][j]=Math.max(a[i-1][j-1],a[i-1][j])+1;
                        a[i][j]=a[i-1][j-1]+1;
                    }
                    else a[i][j]=Math.max(a[i][j-1],a[i-1][j]);
                }
            }
            System.out.println(a[x.length()][y.length()]);
        }
    }

}
