import java.util.Scanner;

public class SâpXepNoiBot {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
//        int t
//                =Integer.valueOf(sc.nextLine())
//                ;
//        while(t-->0){
        int n=sc.nextInt();
        int[] a=new int[n];
        for(int i=0;i<n;i++)
            a[i]=sc.nextInt();
        int count=1;
        for(int i=0;i<n-1;i++){
            for(int j=0;j<n-1;j++){
                if(a[j]>a[j+1]){
                    int tmp=a[j+1];
                    a[j+1]=a[j];
                    a[j]=tmp;
                }
            }
            if(isSorted(a)){
                printfArray(a,count++);
                break;
            }
            else{
                printfArray(a,count++);
            }
        }
//        }
    }

    private static boolean isSorted(int[] a) {
        for(int i=0;i<a.length-1;i++)
            if(a[i]>a[i+1]) return false;
        return true;
    }

    private static void printfArray(int[] a,int buoc) {
        System.out.format("Buoc %d: ",buoc);
        for(int i=0;i<a.length;i++)
            System.out.format("%d ",a[i]);
        System.out.println();
    }
}
