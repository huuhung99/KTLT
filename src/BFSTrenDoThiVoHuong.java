import java.util.*;

public class BFSTrenDoThiVoHuong {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            int s=sc.nextInt();
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            bfs(s);
            System.out.println();
        }
    }

    private static void bfs(int s) {
        Queue<Integer> queue=new ArrayDeque<>();
        queue.add(s); chuaxet[s]=false;
        while (!queue.isEmpty()){
            int v= queue.poll();
            System.out.print( v+" ");
            for(Integer x:lists[v]){
                if(chuaxet[x]){
                    queue.add(x);chuaxet[x]=false;
                }
            }
        }
    }
}
