package A_A_ON_THI.Stack;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;

public class BDN1 {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      long n=sc.nextLong();
      Queue<Long> queue=new ArrayDeque<>();
      int dem=0;
      queue.add(1l);
      while (queue.peek()<=n){
        Long poll = queue.poll();
        dem++;
        queue.add(poll*10);
        queue.add(poll*10+1);
      }
      System.out.println(dem);
    }
  }
}
