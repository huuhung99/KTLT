package A_A_ON_THI.Stack;

import java.util.Scanner;
import java.util.Stack;

public class KiemTraDayNgoacDung {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t= sc.nextInt();
    while (t-->0){
      String s=sc.next();
      boolean check=true;
      Stack<Character> stack=new Stack<>();
      for(int i=0;i<s.length();i++){
        char c = s.charAt(i);
        if(c=='('||c=='{'||c=='['){
          stack.add(c);
          continue;
        }

      }
      if (check&&stack.isEmpty()) {
        System.out.println("YES");
      } else {
        System.out.println("NO");
      }
    }
  }
}
