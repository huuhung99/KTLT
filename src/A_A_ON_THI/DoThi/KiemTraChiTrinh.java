package A_A_ON_THI.DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class KiemTraChiTrinh {
  static int n,m,truoc[]=new int[1001];
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001],check;

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      m=sc.nextInt();
      check=false;
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      for(int i=1;i<=n;i++){
        if(chuaxet[i]){
          dfs(i);
        }
        if(check){
          break;
        }
      }
      if (check) {
        System.out.println("YES");
      } else {
        System.out.println("NO");
      }
    }
  }

  private static void dfs(int i) {
    chuaxet[i]=false;
    for(Integer integer:list[i]){
      if(chuaxet[integer]){
        truoc[integer]=i;
        dfs(integer);
      }
      else if(integer!=truoc[i]){
        check=true;
      }
    }
  }
}
