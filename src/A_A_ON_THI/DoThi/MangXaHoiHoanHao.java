package A_A_ON_THI.DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class MangXaHoiHoanHao {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[100001],check;
  static boolean chuaxet[]=new boolean[100001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      m=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      boolean kq=true;
      for(int i=1;i<=n;i++){
        if(chuaxet[i]){
          check=new ArrayList<>();
          dfs(i);
        }
        for (int j=0;j< check.size();j++){
          if(list[check.get(j)].size()!= check.size()-1){
            kq=false;
            i=n+3;
            break;
          }
        }
      }
      if (kq) {
        System.out.println("YES");
      } else {
        System.out.println("NO");
      }
    }
  }

  private static void dfs(int i) {
    chuaxet[i]=false;
    check.add(i);
    for(Integer integer:list[i]){
      if(chuaxet[integer]){
        dfs(integer);
      }
    }
  }
}
