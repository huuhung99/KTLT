package A_A_ON_THI.DoThi;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class BFSTrenDoThiVoHuong {
  static int n,m,dau;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      m=sc.nextInt();
      dau=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      bfs(dau);
    }
  }

  private static void bfs(int dau) {
    chuaxet[dau]=false;
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(dau);
    System.out.print(dau+" ");
    while (!queue.isEmpty()){
      int v= queue.remove();
      for(Integer integer:list[v]){
        if(chuaxet[integer]){
          queue.add(integer);
          chuaxet[integer]=false;
          System.out.print(integer+" ");
        }
      }
    }
    System.out.println();
  }
}
