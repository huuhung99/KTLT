package A_A_ON_THI.DoThi;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class DemThanPhanLienThongTN06002 {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[20001];
  static boolean chuaxet[]=new boolean[20001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    n=sc.nextInt();
    m=sc.nextInt();
    for(int i=0;i<=n;i++){
      list[i]=new ArrayList<>();
      chuaxet[i]=true;
    }
    for(int i=1;i<=m;i++){
      int u=sc.nextInt();
      int v=sc.nextInt();
      list[u].add(v);
      list[v].add(u);
    }
    for(int i=1;i<=n;i++){
      int dem=0;
      for(int j=1;j<=n;j++) chuaxet[j]=true;
      chuaxet[i]=false;
      for(int j=1;j<=n;j++){
        if(chuaxet[j]){
          bfs(j);
          dem++;
        }
      }
      System.out.println(dem);
    }
  }

  private static void bfs(int j) {
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(j);
    chuaxet[j]=false;
    while (!queue.isEmpty()){
      int v=queue.remove();
      for(Integer integer:list[v]){
        if(chuaxet[integer]){
          chuaxet[integer]=false;
          queue.add(integer);
        }
      }
    }
  }

}
