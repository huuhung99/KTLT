package A_A_ON_THI.DequyVaQHD;

import java.util.Scanner;

public class ToHopCnK {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int c[][]=new int[1001][1001];
    for(int i=0;i<1001;i++){
      for(int j=0;j<=i;j++){
        if(i==0||j==0) c[i][j]=1;
        else c[i][j]=(c[i-1][j]+c[i-1][j-1])%((int)1e9+7);
      }
    }
    int t=sc.nextInt();
    while (t-->0){
      int n=sc.nextInt();
      int k=sc.nextInt();
      System.out.println(c[n][k]);
    }
  }
}
