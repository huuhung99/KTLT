package A_A_ON_THI.DequyVaQHD;

import java.util.Scanner;

public class DoiChoCacChuSo {
  static char[] s,ss;
  static int k;

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      k=sc.nextInt();
      s=sc.next().toCharArray();
      ss=new char[s.length];
      copy(ss,s);
      quaylui(k);
      System.out.println(ss);
    }
  }

  private static void copy(char[] ss, char[] s) {
    for(int i=0;i<s.length;i++){
      ss[i]=s[i];
    }
  }

  private static void quaylui(int k) {
    if(k==0) return;
    for(int i=0;i<s.length-1;i++){
      for(int j=i+1;j<s.length;j++){
        if(s[j]>s[i]){
          char tmp=s[j];
          s[j]=s[i];
          s[i]=tmp;
          if(String.valueOf(s).compareTo(String.valueOf(ss))>0) copy(ss,s);
          quaylui(k-1);
          tmp=s[j];
          s[j]=s[i];
          s[i]=tmp;
        }
      }
    }
  }
}
