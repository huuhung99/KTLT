package A_A_ON_THI.DequyVaQHD;

import java.util.Arrays;
import java.util.Scanner;

public class ToHopTiepTheo {

  static int n;
  static int k;
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int t = sc.nextInt();
    while (t-- > 0) {
      int dem = 0;
      n = sc.nextInt();
      k = sc.nextInt();
      int[] a = new int[k];
      for (int i = 0; i < k; i++) {
        a[i] = sc.nextInt();
      }
      int[] b=Arrays.copyOf(a,a.length);
      int i = k - 1;
      while (i >= 0 && a[i] == n - k + i + 1) {
        i--;
      }
      if (i >= 0) {
        a[i] = a[i] + 1;
        for (int j = i + 1; j < k; j++) {
          a[j] = a[j - 1] + 1;
        }
      } else {
        System.out.println(k);
        continue;
      }
      for (int j = 0; j < k; j++) {
        if (check(b, a[j])) {
          dem++;
        }
      }
      System.out.println(k - dem);
    }
  }

  private static boolean check(int[] a, int x) {
    for (int i = 0; i < k; i++) {
      if (x == a[i]) {
        return true;
      }
    }
    return false;
  }
}
