package A_A_ON_THI.DequyVaQHD;

import java.util.Scanner;

public class DiChuyenTrongMeCung {
  static boolean check;
  static int a[][],n;
  static char kq[];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      a=new int[n+2][n+2];
      kq=new char[1000];
      check=false;
      for(int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
          a[i][j]=sc.nextInt();
        }
      }
      if (a[1][1] == 0) {
        System.out.println(-1);
      }
      else{
        quaylui(1,1,0);
        if (!check) {
          System.out.print(-1);
        }
        System.out.println();
      }
    }
  }

  private static void quaylui(int i, int j,int count) {
    if(i==n&&j==n){
      xuly(count);
      return;
    }
    if(a[i+1][j]==1&&i+1<=n){
      kq[count]='D';
      quaylui(i+1,j, count+1);
    }
    if(a[i][j+1]==1&&j+1<=n){
      kq[count]='R';
      quaylui(i,j+1, count+1);
    }
  }

  private static void xuly(int count) {
    check=true;
    for(int i=0;i<count;i++){
      System.out.print(kq[i]);
    }
    System.out.print(" ");
  }

}
