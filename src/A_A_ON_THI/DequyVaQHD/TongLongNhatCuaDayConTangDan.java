package A_A_ON_THI.DequyVaQHD;

import java.util.Arrays;
import java.util.Scanner;

public class TongLongNhatCuaDayConTangDan {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t= sc.nextInt();
    while (t-->0){
      int n=sc.nextInt();
      int a[]=new int[n];
      int b[]=new int[n];
      for(int i=0;i<n;i++) a[i]=sc.nextInt();
      for(int i=0;i<n;i++){
        b[i]=a[i];
        for(int j=0;j<=i;j++){
          if(a[i]>a[j]){
            b[i]=Math.max(b[i],b[j]+a[i]);
          }
        }
      }
      System.out.println(Arrays.stream(b).max().getAsInt());
    }
  }
}
