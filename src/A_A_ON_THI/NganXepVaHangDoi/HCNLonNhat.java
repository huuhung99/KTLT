package A_A_ON_THI.NganXepVaHangDoi;

import java.util.Scanner;
import java.util.Stack;

public class HCNLonNhat {
  static Scanner sc =new Scanner(System.in);
  static int t,n,i;
  static int[] r,l;
  static long kq;
  static long[] a;
  static Stack<Integer> stack;
  public static void main(String[] args) {
    t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      a=new long[n];
      for(int i=0;i<n;i++) a[i]=sc.nextInt();
      kq=0;
      stack=new Stack<>();
      r = new int[n+1];
      l=new int[n+1];
      for(i=0;i<n;i++){
        while (!stack.isEmpty()&&a[i]<=a[stack.peek()]) stack.pop();
        if(stack.isEmpty()) l[i]=-1;
        else l[i]=stack.peek();
        stack.push(i);
      }
      stack.clear();
      for(i=n-1;i>=0;i--){
        while (!stack.isEmpty()&&a[i]<=a[stack.peek()]) stack.pop();
        if(stack.isEmpty()) r[i]=n;
        else r[i]=stack.peek();
        stack.push(i);
      }
      for(i=0;i<n;i++){
        kq=Math.max(kq,a[i]*(r[i]-l[i]-1));
      }
      System.out.println(kq);
    }
  }
}
