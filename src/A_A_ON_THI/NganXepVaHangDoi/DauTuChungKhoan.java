package A_A_ON_THI.NganXepVaHangDoi;

import java.util.Scanner;
import java.util.Stack;

public class DauTuChungKhoan {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      int n=sc.nextInt();
      int a[]=new int[n];
      for(int i=0;i<n;i++)
        a[i]=sc.nextInt();
      xuly(a,n);
    }
  }

  private static void xuly(int[] a, int n) {
    Stack<Integer> stack=new Stack<>();
    int l[]=new int[n];
    for(int i=0;i<n;i++){
      while (!stack.isEmpty()&&a[i]>=a[stack.peek()]) stack.pop();
      if(stack.isEmpty()) l[i]=-1;
      else l[i]= stack.peek();
      stack.push(i);
    }
    for(int i=0;i<n;i++){
      System.out.print((i-l[i])+" ");
    }
    System.out.println();
  }
}
