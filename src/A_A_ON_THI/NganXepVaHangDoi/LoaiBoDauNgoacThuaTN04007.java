package A_A_ON_THI.NganXepVaHangDoi;

import java.io.*;
import java.util.*;

public class LoaiBoDauNgoacThuaTN04007 {
  public static void main(String[] args) throws FileNotFoundException {
    Scanner sc=new Scanner(System.in);
//    sc=new Scanner(new File("C:\\Users\\huuhung\\Desktop\\SPOJ\\src\\A_A_ON_THI\\NganXepVaHangDoi\\input.txt"));
    int t=Integer.parseInt(sc.nextLine());
    while (t-->0){
      String s=loaiBoDauCach(sc.nextLine());
      boolean danhdau[]=new boolean[s.length()];
      Stack<Integer> stack=new Stack<>();
      for(int i=0;i<s.length();i++){
        danhdau[i]=true;
        if(s.charAt(i)=='('){
          stack.push(i);
        }
        else if(s.charAt(i)==')'){
          Integer pop = stack.pop();
          if(kiemTraKhongCoToanTu(s,pop,i)){
            danhdau[i]=false;
            danhdau[pop]=false;
          }
        }
      }
      for(int i=0;i<s.length();i++){
        if (danhdau[i]) {
          System.out.print(s.charAt(i)+"");
        }
      }
      System.out.println();
    }
  }

  private static boolean kiemTraKhongCoToanTu(String s, Integer i, int k) {
    if(i==0) return true;
    if(s.charAt(i-1)=='-'){
      for(int d=i+1;d<k;d++){
        if(s.charAt(d)=='+'||s.charAt(d)=='-') return false;
      }
    }
    return true;
  }

  private static String loaiBoDauCach(String s) {
    StringBuilder tmp= new StringBuilder();
    for(int i=0;i<s.length();i++){
      if(s.charAt(i)!=32) tmp.append(s.charAt(i));
    }
    return tmp.toString();
  }
}
