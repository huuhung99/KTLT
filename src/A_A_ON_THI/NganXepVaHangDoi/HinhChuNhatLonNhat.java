package A_A_ON_THI.NganXepVaHangDoi;

import java.util.Scanner;
import java.util.Stack;

public class HinhChuNhatLonNhat {
  static long[] a;
  static int[] r,l;
  static int n,t;
  static long kq;
  static Stack<Integer> stack;
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    t=sc.nextInt();
    while (t-->0){
      n= sc.nextInt();
      a=new long[n];
      for(int i=0;i<n;i++)
        a[i]=sc.nextInt();
      r=new int[n];
      l=new int[n];
      kq=-1;
      stack=new Stack<>();
      for(int i=0;i<n;i++){
        while (!stack.isEmpty()&&a[i]<=a[stack.peek()]) stack.pop();
        if(stack.isEmpty()) l[i]=-1;
        else l[i]=stack.peek();
        stack.push(i);
      }

      stack=new Stack<>();
      for(int i=n-1;i>=0;i--){
        while (!stack.isEmpty()&&a[i]<=a[stack.peek()]) stack.pop();
        if(stack.isEmpty()) r[i]=n;
        else r[i]=stack.peek();
        stack.push(i);
      }
      for(int i=0;i<n;i++){
        kq = Math.max(kq,a[i]*(r[i]-l[i]-1));
      }
      System.out.println(kq);
    }
  }
}
