package A_A_ON_THI.NganXepVaHangDoi;

import java.util.Scanner;
import java.util.Stack;

public class BieuThucDungTN30 {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=Integer.parseInt(sc.nextLine());
    while (t-->0){
      String s=sc.nextLine();
      System.out.println(solve(s));
    }
  }

  private static int solve(String s) {
    Stack<Character> stack=new Stack<>();
    int kq=0;
    for(int i=0;i<s.length();i++){
      if(stack.isEmpty()) stack.push(s.charAt(i));
      else if(stack.peek() == '[' && s.charAt(i) == ']'){
        stack.pop();
      }
      else if (stack.peek() == ']' && s.charAt(i) == '[') {
        kq += stack.size();
        stack.pop();
      } else {
        stack.push(s.charAt(i));
      }
    }
    return kq;
  }
}
