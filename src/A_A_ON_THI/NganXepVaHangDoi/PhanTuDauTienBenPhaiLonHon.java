package A_A_ON_THI.NganXepVaHangDoi;

import java.util.Scanner;
import java.util.Stack;

public class PhanTuDauTienBenPhaiLonHon {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      int n=sc.nextInt();
      int a[]=new int[n];
      for(int i=0;i<n;i++)
        a[i]=sc.nextInt();
      int r[]=new int[n];
      Stack<Integer> stack=new Stack<>();
      for(int i=n-1;i>=0;i--){
        while (!stack.isEmpty()&& a[i]>=stack.peek()) stack.pop();
        if(stack.isEmpty()) r[i]=-1;
        else r[i]=stack.peek();
        stack.push(a[i]);
      }
      for(int i=0;i<n;i++){
        System.out.print(r[i]+" ");
      }
      System.out.println();
    }
  }

}
