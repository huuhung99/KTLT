import java.util.Scanner;

public class LuyThua {
    private static long m=1000000007;
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t
                =sc.nextInt()
                ;
        long n,k;
        while(t-->0){
            n=sc.nextLong();
            k= sc.nextLong();
            System.out.println(pow(n,k));
        }
    }

    private static long pow(long n, long k) {
        if(k==1) return n;
        if(k%2==0) return pow(n*n%m,k/2)%m;
        return ((n%m)*pow(n*n%m,k/2)%m)%m;
    }
}
