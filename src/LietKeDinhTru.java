import java.util.ArrayList;
import java.util.Scanner;

public class LietKeDinhTru {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            int dem=0;
            for(int i=1;i<=n;i++){
                if(chuaxet[i]){
                    dfs(i);
                    dem++;
                }
            }
            for(int k=1;k<=n;k++){
                for(int i=0;i<=n;i++) chuaxet[i]=true;
                int tplt=0;
                chuaxet[k]=false;
                for(int i=1;i<=n;i++){
                    if(chuaxet[i]){
                        dfs(i);
                        tplt++;
                    }
                }
                if(tplt!=dem) System.out.print(k+" ");
            }
            System.out.println();
        }
    }

    private static void dfs(int i) {
        chuaxet[i]=false;
        for(Integer integer:lists[i]){
            if(chuaxet[integer]){
                chuaxet[integer]=false;
                dfs(integer);
            }
        }
    }
}
