import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class DuongDiTheoBFSTrenDoThiVoHuong {
    static int n,m,dau,dich,truoc[]=new int[1001];
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();dich=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                truoc[i]=0;
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            bfs(dau);
            if(chuaxet[dich]) System.out.print(-1);
            else {
                ArrayList<Integer> tmp=new ArrayList<>();
                tmp.add(dich);
                while (truoc[dich]!=dau){
                    tmp.add(truoc[dich]);
                    dich=truoc[dich];
                }
                tmp.add(dau);
                for(int i=tmp.size()-1;i>=0;i--)
                    System.out.print(tmp.get(i)+" ");
            }
            System.out.println();
        }
    }

    private static void bfs(int dau) {
        Queue<Integer> queue=new ArrayDeque<>();
        queue.add(dau);
        chuaxet[dau]=false;
        while (!queue.isEmpty()){
            int v=queue.poll();
            chuaxet[v]=false;
            for(Integer i:lists[v]){
                if(chuaxet[i]){
                    queue.add(i);
                    truoc[i]=v;
                    chuaxet[i]=false;
                }
            }
        }
    }
}
