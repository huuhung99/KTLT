import java.util.Arrays;
import java.util.Scanner;

public class DayConTangCoTongSoNguyenTo {
    static int c[],a[],b[],n;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            a=new int[n];c=new int[20];b=new int[20];
            for(int i=0;i<n;i++) a[i]=sc.nextInt();
            Arrays.sort(a);
            for(int i=1;i<=n;i++) c[i]=a[n-i];
            quaylui(1);
        }
    }

    private static void quaylui(int i) {
        for(int j=0;j<=1;j++){
            b[i]=j;
            if(i==n) xuly();
            else quaylui(i+1);
        }
    }

    private static void xuly() {
        int sum=0,i;
        for(i=1;i<=n;i++) sum+=c[i]*b[i];
        if(isPrime(sum)){
            for(i=1;i<=n;i++){
                if(b[i]==1)
                    System.out.print(c[i]+" ");
            }
            System.out.println();
        }
    }

    private static boolean isPrime(int n){
        if(n<2) return false;
        if(n<4) return true;
        if(n%2==0||n%3==0)
            return false;
        for(int i=5;i*i<=n;i+=6)
            if(n%i==0||n%(i+2)==0)
                return false;
        return true;
    }
}
