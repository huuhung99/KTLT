package SinhVien;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SinhVien {
    private String maSv;
    private String ten;
    private String lop;
    private Date ngaySinh;
    private Float diem;

    public SinhVien(String maSv, String ten, String lop, String ngaySinh, Float diem) throws ParseException {
        this.maSv = maSv;
        this.ten = ten;
        this.lop = lop;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        this.diem = diem;
    }
    public String toString(){
        return String.format("B20DCCN001 %s %s %s %.2f",ten,lop,new SimpleDateFormat("dd/MM/yyyy").format(ngaySinh),diem);
    }
}
