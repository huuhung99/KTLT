package TimThuKhoaKiThi;

public class ThiSinh{
    private String name,ngaySinh;
    private int ma;
    private double a,b,c,tongDiem;

    public ThiSinh(int ma,String name, String ngaySinh,  double a, double b, double c) {
        this.name = name;
        this.ngaySinh = ngaySinh;
        this.ma = ma;
        this.a = a;
        this.b = b;
        this.c = c;
        tongDiem=Math.round((a+b+c)*10.0)/10.0;
    }

    public double getTongDiem() {
        return tongDiem;
    }

    @Override
    public String toString() {
        return String.format("%d %s %s %.1f",ma,name,ngaySinh,tongDiem);
    }
}
