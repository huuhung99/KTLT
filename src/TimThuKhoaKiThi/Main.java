package TimThuKhoaKiThi;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<ThiSinh> list=new ArrayList<>();
        double max=0.0;
        for(int i=1;i<=t;i++){
            ThiSinh thiSinh = new ThiSinh(i, sc.nextLine(), sc.nextLine(), Double.parseDouble(sc.nextLine())
                    , Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine()));
            if(thiSinh.getTongDiem()>=max){
                max=thiSinh.getTongDiem();
                list.add(thiSinh);
            }
        }
        for(ThiSinh thiSinh:list){
            if(thiSinh.getTongDiem()==max)
                System.out.println(thiSinh);
        }
    }
}
