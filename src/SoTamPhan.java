import java.util.Scanner;

public class SoTamPhan {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            long n=sc.nextLong();
            boolean check=true;
            while(n>0){
                if(n%10>2){
                    System.out.println("NO");
                    check=false;
                    break;
                }
                n/=10;
            }
            if(check) System.out.println("YES");
        }
    }
}
