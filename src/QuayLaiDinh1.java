import java.util.*;

public class QuayLaiDinh1 {
    static int n,m,truoc[]=new int[21];
    static ArrayList<Integer>[] lists=new ArrayList[21];
    static boolean chuaxet[]=new boolean[21];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v= sc.nextInt();
                lists[u].add(v);
            }
//            for(int i=0;i<=n;i++){
//                Collections.sort(lists[i]);
//            }
            int dau=1,dich=2;
            Set<Integer> set=new HashSet<>();
            bfs(dau);
            set.add(dich);
            while (truoc[dich]!=dau){
                set.add(truoc[dich]);
                dich=truoc[dich];
            }
            set.add(dau);
            dau=2;dich=1;
            bfs(dau);
            set.add(dich);
            while (truoc[dich]!=dau){
                set.add(truoc[dich]);
                dich=truoc[dich];
            }
            set.add(dau);
            System.out.println(set.size());
        }
    }

    private static void bfs(int i) {
        for(int j=0;j<21;j++){
            chuaxet[j]=true;
            truoc[j]=0;
        }
        Queue<Integer> queue=new ArrayDeque<>();
        queue.add(i);
        while (!queue.isEmpty()){
            int v=queue.poll();
            chuaxet[v]=false;
            for (Integer integer:lists[v]){
                if(chuaxet[integer]){
                    truoc[integer]=v;
                    queue.add(integer);
                    chuaxet[integer]=false;
                }
            }
        }
    }
}
