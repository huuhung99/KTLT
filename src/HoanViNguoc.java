import java.util.Scanner;

public class HoanViNguoc {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt();
            int[] a=new int[n];
            for(int i=0;i<n;i++)
                a[i]=n-i;
            for(int k=0;k< a.length;k++)
                System.out.print(a[k]);
            System.out.print(" ");
            while(prev_permutation(a)){
                for(int k=0;k< a.length;k++)
                    System.out.print(a[k]);
                System.out.print(" ");
            };
            System.out.println();
        }
    }

    private static boolean prev_permutation(int[] arr) {
        int len = arr.length;
        int i = len - 1;
        while (i > 0) {
            if (arr[i - 1] > arr[i]) break;
            i--;
        }
        if (i <= 0) return false;
        int j = len - 1;
        while (j >= i) {
            if (arr[i - 1] > arr[j]) break;
            j--;
        }
        int tmp=arr[i-1];
        arr[i-1]=arr[j];
        arr[j]=tmp;
        len--;
        while (i < len) {
            tmp=arr[i];
            arr[i]=arr[len];
            arr[len]=tmp;
            len--;
            i++;
        }
        return true;
    }
}
