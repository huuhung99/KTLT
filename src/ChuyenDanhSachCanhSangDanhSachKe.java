import java.util.ArrayList;
import java.util.Scanner;

public class ChuyenDanhSachCanhSangDanhSachKe {
    static int n,m;
    static ArrayList<Integer>[] list=new ArrayList[1001];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++){
                list[i]=new ArrayList<>();
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                list[u].add(v);
                list[v].add(u);
            }
            for(int i=1;i<=n;i++){
                System.out.print(i+":");
                for(int j=0;j<list[i].size();j++)
                    System.out.print(" "+list[i].get(j));
                System.out.println();
            }
        }
    }
}
