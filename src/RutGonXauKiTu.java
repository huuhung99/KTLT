import java.util.Scanner;

public class RutGonXauKiTu {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        String chars = sc.nextLine();
        System.out.println(solve(chars));
    }

    private static String solve(String chars) {
        boolean check=false;
        for(int i=0;i<chars.length()-1;i++){
            if(chars.charAt(i)==chars.charAt(i+1)){
                check=true;
                chars=chars.substring(0,i)+chars.substring(i+2,chars.length());
            }
        }
        if(chars.length()==0) return "Empty String";
        if(check==false) return new String(chars);
        else return solve(chars);
    }
}
