package SapXepSinhVienTheoLop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<SinhVien> list=new ArrayList<>();
        while (t-->0){
            SinhVien sinhVien=new SinhVien(sc.nextLine(), sc.nextLine(), sc.nextLine(),sc.nextLine());
            list.add(sinhVien);
        }
        Collections.sort(list);
        for(SinhVien sinhVien:list)
            System.out.println(sinhVien);
    }
}
