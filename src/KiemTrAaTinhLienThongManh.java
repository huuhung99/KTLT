import java.util.ArrayList;
import java.util.Scanner;

public class KiemTrAaTinhLienThongManh {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t= sc.nextInt();
        while (t-->0){
            n=sc.nextInt();m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
            }
            boolean check=true;
            dfs(1);
            for(int i=1;i<=n;i++){
                if(chuaxet[i]){
                    check=false;
                    break;
                }
            }
            if(check) System.out.println("YES");
            else System.out.println("NO");
        }
    }

    private static void dfs(int i) {
        chuaxet[i]=false;
        for(Integer k:lists[i]){
            if(chuaxet[k]){
                chuaxet[k]=false;dfs(k);
            }
        }
    }
}
