package SapXepSinhVienTheoMaSinhVien;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        List<SinhVien> list=new ArrayList<>();
        while (sc.hasNext()){
            SinhVien sinhVien=new SinhVien(sc.nextLine(), sc.nextLine(), sc.nextLine(),sc.nextLine());
            list.add(sinhVien);
        }
        Collections.sort(list);
        for(SinhVien sinhVien:list)
            System.out.println(sinhVien);
    }
}
