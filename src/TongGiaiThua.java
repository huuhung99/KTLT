import java.util.Scanner;

public class TongGiaiThua {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        long[] a=new long[21];
        for(long i=1;i<21;i++)
            a[(int)i]=i;
        long[] giaiThua=new long[21];
        giaiThua[1]=1;
        for(int i=2;i<21;i++){
            giaiThua[i]=giaiThua[i-1]*i;
        }
        long[] tongGiaiThua=new long[21];
        tongGiaiThua[1]=1;
        for(int i=2;i<21;i++){
            tongGiaiThua[i]=giaiThua[i-1]*a[i]+tongGiaiThua[i-1];
        }
        System.out.println(tongGiaiThua[sc.nextInt()]);
    }
}
