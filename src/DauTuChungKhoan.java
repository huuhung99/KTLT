import java.util.Scanner;
import java.util.Stack;

public class DauTuChungKhoan {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t= sc.nextInt();
        while (t-->0){
            int n=sc.nextInt(),i, a[]=new int[n];
            for(i=0;i<n;i++) a[i]=sc.nextInt();
            int l[]=new int[n];
            Stack<Integer> stack=new Stack<>();
            for(i=0;i<n;i++){
                while (!stack.isEmpty()&&a[i]>=a[stack.peek()]) stack.pop();
                if(stack.isEmpty()) l[i]=-1;
                else l[i]=stack.peek();
                stack.push(i);
            }
            for(i=0;i<n;i++){
                if(l[i]==-1) System.out.print("1 ");
                else System.out.format("%d ",i-l[i]);
            }
            System.out.println();
        }
    }
}
