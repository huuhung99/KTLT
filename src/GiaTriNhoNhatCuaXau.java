import java.util.*;

public class GiaTriNhoNhatCuaXau {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        while (t-->0){
            long n=Long.parseLong(sc.nextLine());
            String s= sc.nextLine();
            Map<Character,Long> map=new HashMap<>();
            for(int i=0;i<s.length();i++){
                if(map.containsKey(s.charAt(i))){
                    map.put(s.charAt(i),map.get(s.charAt(i))+1);
                }
                else map.put(s.charAt(i),1l);
            }
            Queue<Long> queue=new PriorityQueue<>(Collections.reverseOrder());
            for(Map.Entry e:map.entrySet()){
                queue.add((Long) e.getValue());
            }
            while (n-->0){
                queue.add(queue.poll()-1);
            }
            long result=0;
            while (!queue.isEmpty()){
                result+=Math.pow(queue.poll(),2);
            }
            System.out.println(result);
        }
    }
}
