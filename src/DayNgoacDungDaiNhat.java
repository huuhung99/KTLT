import java.util.Scanner;
import java.util.Stack;

public class DayNgoacDungDaiNhat {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        while(t-->0){
            String s=sc.nextLine();
            System.out.println(tinh(s));
        }
    }

    private static int tinh(String s) {
        Stack<Integer> stack=new Stack<>();
        stack.push(-1);
        int kq=0;
        for(int i=0;i<s.length();i++){
            char c = s.charAt(i);
            if(c=='(') stack.push(i);
            else{
                stack.pop();
                if(!stack.empty()){
                    kq=Math.max(kq,i- stack.peek());
                }
                else stack.push(i);
            }
        }
        return kq;
    }
}
