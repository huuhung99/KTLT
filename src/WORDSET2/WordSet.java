package WORDSET2;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

class WordSet{
  private Set<String> set=new TreeSet<>();

  public WordSet() {
  }

  public WordSet(String s) {
    set.addAll(Arrays.asList(s.trim().toLowerCase().split("\\s+")));
  }

  public String union(WordSet s2) {
    Set<String> tmp=new TreeSet();
    tmp.addAll(set);
    tmp.addAll(s2.set);
    String kq="";
    for(String s : tmp){
      kq+=s+" ";
    }
    return kq;
  }

  public String intersection(WordSet s2) {
    Set<String> tmp=new TreeSet<>();
    tmp.addAll(set);
    tmp.retainAll(s2.set);
    String kq="";
    for(String s : tmp){
      kq+=s+" ";
    }
    return kq;
  }
}