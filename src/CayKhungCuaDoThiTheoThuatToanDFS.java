import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class CayKhungCuaDoThiTheoThuatToanDFS {
    static int n,m,truoc[]=new int[1001],s;
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];
    static ArrayList<String> kq;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc= new Scanner(System.in);
//        sc=new Scanner(new File("C:\\Users\\huuhung\\Desktop\\SPOJ\\src\\input2.txt"));
        int t= sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            s=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            kq=new ArrayList<>();
            dfs(s);
            if (kq.size() == n - 1) {
                for (String tmp : kq) {
                    System.out.println(tmp);
                }
            } else {
                System.out.println(-1);
            }
        }
    }

    private static void dfs(int s) {
        chuaxet[s]=false;
        for(Integer integer: lists[s]){
            if(chuaxet[integer]){
                kq.add(s+" "+integer);
                chuaxet[integer]=false;
                dfs(integer);
            }
        }
    }
}
