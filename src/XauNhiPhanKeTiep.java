import java.util.Scanner;

public class XauNhiPhanKeTiep {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int n=Integer.valueOf(sc.nextLine());
        while (n-->0){
            System.out.println(nextGreater(sc.nextLine()));
        }
    }

    private static String nextGreater(String num) {
        int l = num.length();
        int i;
        for (i = l - 1; i >= 0; i--) {
            if (num.charAt(i) == '0') {
                num = num.substring(0, i) + '1' + num.substring(i+1);
                break;
            }
            else {
                num = num.substring(0, i) + '0' + num.substring(i + 1);
            }
        }
        return num;
    }
}
