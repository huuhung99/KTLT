import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class DuongDiTheoBFS {
  static int n,m,dau,truoc[]=new int[1001];
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();
      for(int i=1;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      bfs(dau);
      for(int i=1;i<=n;i++){
        if(chuaxet[i]){
          System.out.println("No path");
        }
        else if(i==dau) continue;
        else{
          ArrayList<Integer> kq=new ArrayList<>();
          int dich=i;
          kq.add(dich);
          while (truoc[dich]!=dau){
            kq.add(truoc[dich]);
            dich=truoc[dich];
          }
          kq.add(dau);
          for(int j= kq.size()-1;j>=0;j--){
            System.out.print(kq.get(j)+" ");
          }
          System.out.println();
        }
      }
    }
  }

  private static void bfs(int dau) {
    chuaxet[dau]=false;
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(dau);
    while (!queue.isEmpty()){
      int v=queue.poll();
      chuaxet[v]=false;
      for(Integer integer:list[v]){
        if(chuaxet[integer]){
          chuaxet[integer]=false;
          truoc[integer]=v;
          queue.add(integer);
        }
      }
    }
  }
}
