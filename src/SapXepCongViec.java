import java.util.*;

public class SapXepCongViec {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t
                =sc.nextInt()
                ;
        int result=1;
        while(t-->0){
            int len=sc.nextInt();
            Activity arr[] = new Activity[len];
            for(int i=0;i<len;i++){
                arr[i]=new Activity(sc.nextInt(),sc.nextInt());
            }
            System.out.println(printMaxActivities(arr,len,result));
            result=1;
        }
    }

    static int printMaxActivities(Activity arr[], int n,int result){
        Arrays.sort(arr, new Comparator<Activity>() {
            @Override
            public int compare(Activity s1, Activity s2) {
                return s1.finish - s2.finish;
            }
        });
        int i = 0;
        for (int j = 1; j < n; j++){
            if (arr[j].start >= arr[i].finish){
                result++;
                i = j;
            }
        }
        return result;
    }
}
class Activity{
    int start, finish;
    public Activity(int start, int finish){
        this.start = start;
        this.finish = finish;
    }
}
