import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class MangXaHoiHoanHao {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[100005];
    static boolean chuaxet[]=new boolean[100005];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n= sc.nextInt();
            m= sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v= sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            bfs(1);
            boolean check=true;
            for(int i=1;i<=n;i++){
                if(chuaxet[i]){
                    check=false;
                    break;
                }
            }
            if(check) System.out.println("YES");
            else System.out.println("NO");
        }
    }

    private static void bfs(int i) {
        Queue<Integer> queue=new ArrayDeque<>();
        queue.add(i);
        while (!queue.isEmpty()){
            int v=queue.poll();
            chuaxet[v]=false;
            for(Integer integer:lists[v]){
                if(chuaxet[integer]){
                    queue.add(integer);
                    chuaxet[integer]=false;
                }
            }
        }
    }
}
