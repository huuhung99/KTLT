import java.util.Scanner;

public class TroChoiVoiCacConSo {
    static int c[],n;
    static boolean check[];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            c=new int[n+1];
            check=new boolean[n+1];
            quaylui(1);
        }
    }

    private static void quaylui(int i) {
        for(int j=1;j<=n;j++){
            if(!check[j]){
                check[j]=true;
                c[i]=j;
                if(i==n) xuly();
                else quaylui(i+1);
                check[j]=false;
            }
        }
    }

    private static void xuly() {
        boolean check=true;
        for(int i=1;i<n;i++)
            if(Math.abs(c[i+1]-c[i])==1){
                check=false;
                break;
            }
        if(check==true){
            for (int i=1;i<n+1;i++)
                System.out.print(c[i]);
            System.out.println();
        }
    }
}
