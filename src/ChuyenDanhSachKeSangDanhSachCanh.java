import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ChuyenDanhSachKeSangDanhSachCanh {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc=new Scanner(System.in);
//        sc=new Scanner(new File("C:\\Users\\huuhung\\Desktop\\SPOJ\\src\\input.txt"));
        int m=Integer.valueOf(sc.nextLine());
        List<Point> set=new ArrayList<>();
        for (int i=1;i<=m;i++){
            String[] split = sc.nextLine().trim().split(" ");
            for(int j=0;j<split.length;j++){
                int tmp=Integer.valueOf(split[j]);
                if(tmp>i)
                    set.add(new Point(i,tmp));
            }
        }//        Collections.sort(set,(o1, o2) -> {
//            if(o1.u==o2.u){
//                return o1.v<o2.v?-1:1;
//            }
//            return o1.u<o2.u?-1:1;
//        });

        for(Point p:set)
            System.out.println(p);
    }
    public static class Point{
        private int u;
        private int v;

        public Point(int u, int v) {
            this.u = u;
            this.v = v;
        }

        @Override
        public String toString() {
            return u+" "+v;
        }
    }
}
