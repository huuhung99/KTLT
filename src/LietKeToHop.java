import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class LietKeToHop {
  static int n,k,c[];
  static Object[] objects;
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    n=sc.nextInt();
    k=sc.nextInt();
    String s=sc.next();
    Set<Character> set= new TreeSet<>();
    for(int i=0;i<s.length();i++) set.add(s.charAt(i));
    objects=set.toArray();
    n=objects.length;
    c=new int[n+1];
    toHop(1);
  }

  private static void toHop(int i) {
    for(int j=c[i-1]+1;j<=n-k+i;j++){
      c[i]=j;
      if(i==k) xuLy();
      else toHop(i+1);
    }
  }

  private static void xuLy() {
    for(int i=1;i<=k;i++){
      System.out.print(objects[c[i]-1]);
    }
    System.out.println();
  }
}
