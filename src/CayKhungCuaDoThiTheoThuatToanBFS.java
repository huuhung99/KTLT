import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class CayKhungCuaDoThiTheoThuatToanBFS {
  static int n,m,s;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static ArrayList<String> kq;
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();s=sc.nextInt();
      for(int i=0;i <=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      kq=new ArrayList<>();
      bfs(s);
      if (kq.size() == n - 1) {
        for (String tmp : kq) {
          System.out.println(tmp);
        }
      } else {
        System.out.println(-1);
      }
    }
  }

  private static void bfs(int s) {
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(s);
    while (!queue.isEmpty()){
      Integer v = queue.poll();
      chuaxet[v] = false;
      for(Integer integer:list[v]){
        if(chuaxet[integer]){
          chuaxet[integer]=false;
//          if(kq.size()==n-1){
//            queue.clear();
//            break;
//          }
          kq.add(v + " " + integer);
          queue.add(integer);
        }
      }
    }
  }

}
