import java.util.Scanner;

public class TraCuuDonHang {
    private String ten,ma,soThuTuDonHang;
    private long donGia,soLuong,giamGia,thanhTien;

    public TraCuuDonHang(String ten, String ma, long donGia, long soLuong) {
        this.ten = ten;
        this.ma = ma;
        this.soThuTuDonHang=ma.substring(1,4);
        this.donGia = donGia;
        this.soLuong = soLuong;
        if(this.ma.charAt(4)=='2'){
            this.giamGia=(this.donGia*3*this.soLuong)/10;
        }
        else this.giamGia=(this.donGia*this.soLuong)/2;
        thanhTien=this.donGia*this.soLuong-this.giamGia;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %d %d",ten,ma,soThuTuDonHang,giamGia,thanhTien);
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        for(int i=0;i<t;i++){
            System.out.println(new TraCuuDonHang(sc.nextLine(),sc.nextLine()
                    , Long.parseLong(sc.nextLine()), Long.parseLong(sc.nextLine())));
        }
    }
}
