import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class SapXepDonHang implements Comparable<SapXepDonHang>{
    private String ten,ma,soThuTuDonHang;
    private long donGia,soLuong,giamGia,thanhTien;

    public SapXepDonHang(String ten, String ma, long donGia, long soLuong) {
        this.ten = ten;
        this.ma = ma;
        this.soThuTuDonHang=ma.substring(1,4);
        this.donGia = donGia;
        this.soLuong = soLuong;
        if(this.ma.charAt(4)=='2'){
            this.giamGia=(this.donGia*3*this.soLuong)/10;
        }
        else this.giamGia=(this.donGia*this.soLuong)/2;
        thanhTien=this.donGia*this.soLuong-this.giamGia;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %d %d",ten,ma,soThuTuDonHang,giamGia,thanhTien);
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<SapXepDonHang> list=new ArrayList<>();
        for(int i=0;i<t;i++){
            list.add(new SapXepDonHang(sc.nextLine(),sc.nextLine()
                    , Long.parseLong(sc.nextLine()), Long.parseLong(sc.nextLine())));
        }
        Collections.sort(list);
        for(SapXepDonHang sapXepDonHang:list)
            System.out.println(sapXepDonHang);
    }

    @Override
    public int compareTo(SapXepDonHang o) {
        return this.soThuTuDonHang.compareTo(o.soThuTuDonHang);
    }
}
