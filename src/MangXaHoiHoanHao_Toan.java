import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class MangXaHoiHoanHao_Toan {
    static ArrayList<Integer>[] ke = new ArrayList[100001];
    static boolean[] chuaxet = new boolean[100001];
    static ArrayList<Integer> chua = new ArrayList<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        while (t-- > 0) {
            int n = in.nextInt(), m = in.nextInt();
            for (int i = 0; i <= n; i++) {
                ke[i] = new ArrayList<>();
                chuaxet[i] = true;
            }

            for (int i = 1; i <= m; i++) {
                int u = in.nextInt(), v = in.nextInt();
                ke[u].add(v);
                ke[v].add(u);
            }
            boolean check = false;
            for (int i = 1; i <= n; i++) {
                chua.clear();
                if (chuaxet[i]) bfs(i);
                for (int j = 0; j < chua.size(); j++) {
                    if (ke[chua.get(j)].size() != (chua.size() - 1)){
                        check = true;
                        break;
                    }
                }
                if (check) break;
            }
            if (check) System.out.println("NO");
            else System.out.println("YES");

        }
    }

    private static void bfs(int i) {
        Queue<Integer> q = new LinkedList<>();
        q.add(i);
        chuaxet[i] = false;
        while (!q.isEmpty()) {
            int out = q.poll();
            chua.add(out);
            for (Integer integer :
                    ke[out]) {
                if (chuaxet[integer]) {
                    q.add(integer);
                    chuaxet[integer] = false;
                }
            }
        }
    }
}
