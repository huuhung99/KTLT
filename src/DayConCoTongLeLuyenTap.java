import java.util.Arrays;
import java.util.Scanner;

public class DayConCoTongLeLuyenTap {
    static int a[],b[],c[],n;

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            a=new int[n];
            for(int i=0;i<n;i++) a[i]=sc.nextInt();
            Arrays.sort(a);
            b=new int[20];
            c=new int[20];
            for(int i=1;i<=n;i++) b[i]=a[n-i];
            quaylui(1);
        }
    }

    private static void quaylui(int i) {
        for(int j=0;j<=1;j++){
            c[i]=j;
            if(i==n) xuly();
            else quaylui(i+1);
        }
    }

    private static void xuly() {
        int tong=0,i;
        for(i=1;i<=n;i++) tong+=b[i]*c[i];
        if(tong%2==1){
            for(i=1;i<=n;i++){
                if(c[i]==1){
                    System.out.print(b[i]+" ");
                }
            }
            System.out.println();
        }
    }
}
