import java.util.Scanner;

public class GapDoiDaySo {
    public static void main(String[] args) {
        String abc="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            Integer n=sc.nextInt();
            Long k= sc.nextLong();
            System.out.println(abc.charAt(quaylui(n,k)-1));
        }
    }

    private static int quaylui(Integer n, Long k) {
        int x=(int)Math.pow(2,n-1);
        if(k==x) return n;
        else if(k<x) return quaylui(n-1,k);
        return quaylui(n-1,k-x);
    }
}
