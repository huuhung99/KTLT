import java.util.Scanner;

public class TinhLuyThua {
    private static int m=1000000007;
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int n;
        long k;
        while(true){
            n=sc.nextInt();
            k=sc.nextLong();
            if(n==0&&k==0) break;
            System.out.println(pow(n,k));
        }
    }

    private static long pow(int n, long k) {
        if(k==0) return 1;
        long x=pow(n,k/2);
        if(k%2==0) return (x*x)%m;
        return n*(x*x%m)%m;
    }
}
