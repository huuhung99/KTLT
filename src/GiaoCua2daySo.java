import java.util.*;
import java.util.stream.Collectors;

public class GiaoCua2daySo {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt(),b=sc.nextInt();
        List<Integer> list=new ArrayList<>();
        int[] arr=new int[1005];
        for(int i=0;i<1005;i++)
            arr[i]=0;
        for(int i=0;i<a;i++)
            arr[sc.nextInt()]=1;
        Set<Integer> set=new HashSet<>();
        for(int i=0;i<b;i++){
            set.add(sc.nextInt());
        }
        List<Integer> collect = set.stream().collect(Collectors.toList());
        for(int i=0;i<collect.size();i++){
            if(arr[collect.get(i)]==1) list.add(collect.get(i));
        }
        Collections.sort(list);
        for(int i=0;i<list.size();i++){
            System.out.print(list.get(i)+" ");
        }
        System.out.println();
    }
}
