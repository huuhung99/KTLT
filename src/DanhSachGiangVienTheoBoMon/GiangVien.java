package DanhSachGiangVienTheoBoMon;

public class GiangVien {
    private String ma,ten,boMon;

    public GiangVien(int ma, String ten, String boMon) {
        this.ma = "GV"+String.format("%02d",ma);
        this.ten = ten;
        this.boMon = boMon;
    }

    public String getBoMon() {
        return boMon;
    }

    public static String xuly(String boMon) {
        String tmp="";
        String[] split = boMon.trim().toUpperCase().split("\\W+");
        for(int i=0;i<split.length;i++)
            tmp+=String.valueOf(split[i].charAt(0));
        return tmp;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s",ma,ten,xuly(boMon));
    }
}
