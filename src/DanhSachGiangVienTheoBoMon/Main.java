package DanhSachGiangVienTheoBoMon;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<GiangVien> list=new ArrayList<>();
        for(int i=1;i<=t;i++){
            list.add(new GiangVien(i,sc.nextLine(),sc.nextLine()));
        }
        int q=Integer.parseInt(sc.nextLine());
        for(int i=0;i<q;i++){
            String query=sc.nextLine().trim();
            System.out.format("DANH SACH GIANG VIEN BO MON %s:\n",GiangVien.xuly(query));
            for(GiangVien giangVien:list){
                if(query.equalsIgnoreCase(giangVien.getBoMon()))
                    System.out.println(giangVien);
            }
        }
    }
}
