import java.util.Scanner;

public class HoanViNguocDeQuy {
    static int a[],n;
    static boolean b[];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t= sc.nextInt();
        while (t-->0){
            n= sc.nextInt();
            a=new int[n+1];
            b=new boolean[n+1];
            quaylui(1);
            System.out.println();
        }
    }

    private static void quaylui(int i) {
        for(int j=n;j>=1;j--){
            if(!b[j]){
                a[i]=j;
                b[j]=true;
                if(i==n) in();
                else quaylui(i+1);
                b[j]=false;
            }
        }
    }

    private static void in() {
        for(int i=1;i<=n;i++){
            System.out.print(a[i]);
        }
        System.out.print(" ");
    }
}
