import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class MangXaHoiHoanHao_Hung {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[100001],tplt;
  static boolean chuaxet[]=new boolean[100001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v= sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      int i;
      for(i=1;i<=n;i++){
        tplt=new ArrayList<>();
        if(chuaxet[i]){
          bfs(i);
        }
        for(int k=0;k<tplt.size();k++){
          if(list[tplt.get(k)].size() != tplt.size()-1){
            System.out.println("NO");
            i=n+3;
            break;
          }
        }
      }
      if (i == n + 1) {
        System.out.println("YES");
      }
    }
  }

  private static void bfs(int i) {
    Queue<Integer> queue=new ArrayDeque();
    queue.add(i);
    tplt.add(i);
    while (!queue.isEmpty()){
      int v= queue.poll();
      chuaxet[v]=false;
      for(Integer integer : list[v]){
        if(chuaxet[integer]){
          queue.add(integer);
          chuaxet[integer]=false;
          tplt.add(integer);
        }
      }
    }
  }

}
