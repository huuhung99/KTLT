import java.util.ArrayList;
import java.util.Scanner;

public class BieuDienDoThiVoHuong {
    static int n,m;
    static ArrayList<Integer>[] lists=new ArrayList[1001];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++)
                lists[i]=new ArrayList<>();
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
            }
            for(int i=1;i<=n;i++){
                System.out.print(i+":");
                for(int j=0;j<lists[i].size();j++)
                    System.out.print(" "+lists[i].get(j));
                System.out.println();
            }
        }
    }
}
