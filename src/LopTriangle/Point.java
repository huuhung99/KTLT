package LopTriangle;

import java.util.Scanner;

public class Point {
    private double x,y;

    public Point(Scanner sc) {
        x=sc.nextDouble();
        y=sc.nextDouble();
    }

    public static Point nextPoint(Scanner sc) {
        return new Point(sc);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
