package LopTriangle;

public class Triangle {
    private Point a,b,c;
    private double x,y,z;
    public Triangle(Point a, Point b, Point c) {
        this.a=a;
        this.b=b;
        this.c=c;
        x=dodai(a,b);
        y=dodai(b,c);
        z=dodai(a,c);
    }

    private double dodai(Point a, Point b) {
        return Math.sqrt((a.getX()-b.getX())*(a.getX()-b.getX())+
                (a.getY()-b.getY())*(a.getY()-b.getY()));
    }

    public boolean valid() {
        if(x+y>z&&x+z>y&&y+z>x) return true;
        return false;
    }

//    public String getPerimeter() {
//        return String.format("%.3f",Math.round((x+y+z)*1000.0)/1000.0);
//    }
    public String getPerimeter() {
        return String.format("%.3f",x+y+z);
    }
}
