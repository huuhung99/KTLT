package A_LuyenTap_DoThi;

import java.util.Scanner;

public class Floyd {
  static int n,m;
  static int list[][]=new int[101][101];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    n=sc.nextInt();
    m=sc.nextInt();
    for(int i=1;i<=n;i++){
      for(int j=1;j<=n;j++){
        if(i==j) list[i][j]=0;
        else list[i][j]=(int)1e9;
      }
    }
    for(int i=1;i<=m;i++){
      int u=sc.nextInt();int v=sc.nextInt();int w=sc.nextInt();
      list[u][v]=w;
      list[v][u]=w;
    }
    floyd();
    int t=sc.nextInt();
    while (t-->0){
      System.out.println(list[sc.nextInt()][sc.nextInt()]);
    }
  }

  private static void floyd() {
    for(int k=1;k<=n;k++){
      for(int i=1;i<=n;i++){
        for(int j=1;j<=n;j++){
          if(list[i][j]>list[i][k]+list[k][j]){
            list[i][j]=list[i][k]+list[k][j];
          }
        }
      }
    }
  }
}
