package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class DFSTrenDoThiVoHuong {
  static int n,m,dau;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t = sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      dfs(dau);
      System.out.println();
    }
  }

  private static void dfs(int dau) {
    chuaxet[dau]=false;
    System.out.print(dau+" ");
    for(Integer integer: list[dau]){
      if(chuaxet[integer]){
        dfs(integer);
      }
    }
  }

}
