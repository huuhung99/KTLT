package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class LienThong {
  static int n,m,dau;
  static ArrayList<Integer> list[] =new ArrayList[301];
  static boolean chuaxet[] =new boolean[301];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();
    for(int i=1;i<=n;i++){
      list[i]=new ArrayList<>();
      chuaxet[i]=true;
    }
    for(int i=1;i<=m;i++){
      int u=sc.nextInt();
      int v=sc.nextInt();
      list[u].add(v);
      list[v].add(u);
    }
    dfs(dau);
    boolean check=true;
    for(int i=1;i<=n;i++){
      if (chuaxet[i]) {
        System.out.println(i+" ");
        check=false;
      }
    }
    if (check) {
      System.out.println(0);
    }
  }

  private static void dfs(int dau) {
    chuaxet[dau]=false;
    for(Integer integer:list[dau]){
      if(chuaxet[integer]) dfs(integer);
    }
  }

}
