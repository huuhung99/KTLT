package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class MangXaHoiHoanHao {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[100001],tplt;
  static boolean chuaxet[]=new boolean[100001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();
      for(int i=1;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      int i;
      for(i=1;i<=n;i++){
        tplt=new ArrayList<>();
        if(chuaxet[i]){
          dfs(i);
        }
        for(int j=0;j<tplt.size();j++){
          if(list[tplt.get(j)].size()!=tplt.size()-1){
            System.out.println("NO");
            i=n+3;
            break;
          }
        }
      }
      if (i == n + 1) {
        System.out.println("YES");
      }
    }
  }

  private static void dfs(int i) {
    chuaxet[i]=false;
    tplt.add(i);
    for(Integer integer:list[i]){
      if(chuaxet[integer]){
        dfs(integer);
      }
    }
  }
}
