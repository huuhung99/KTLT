package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class DuongDiTheoDFSVoiDoThiVoHuong {
  static int n,m,dau,dich,truoc[]=new int[1001];
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();dich=sc.nextInt();
      for(int i=1;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      dfs(dau);
      if (chuaxet[dich]) {
        System.out.println(-1);
      }
      else{
        ArrayList<Integer> kq=new ArrayList<>();
        kq.add(dich);
        while (truoc[dich]!=dau){
          kq.add(truoc[dich]);
          dich=truoc[dich];
        }
        kq.add(dau);
        for (int i = kq.size() - 1; i >= 0; i--) {
          System.out.print(kq.get(i)+" ");
        }
        System.out.println();
      }
    }
  }

  private static void dfs(int dau) {
    chuaxet[dau]=false;
    for(Integer integer:list[dau]){
      if(chuaxet[integer]){
        truoc[integer]=dau;
        dfs(integer);
      }
    }
  }
}
