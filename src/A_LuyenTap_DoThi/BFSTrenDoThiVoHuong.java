package A_LuyenTap_DoThi;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class BFSTrenDoThiVoHuong {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t = sc.nextInt();
    while (t-->0){
      int n=sc.nextInt();int m=sc.nextInt();int s=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      bfs(s);
      System.out.println();
    }
  }

  private static void bfs(int s) {
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(s);
    chuaxet[s]=false;
    System.out.print(s+" ");
    while (!queue.isEmpty()){
      int v=queue.poll();
      for(Integer integer: list[v]){
        if(chuaxet[integer]){
          System.out.print(integer+ " ");
          chuaxet[integer]=false;
          queue.add(integer);
        }
      }
    }
  }

}
