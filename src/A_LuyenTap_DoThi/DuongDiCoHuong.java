package A_LuyenTap_DoThi;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class DuongDiCoHuong {
  static int n,m,dau,dinh,truoc[]=new int[1001];
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();dau=sc.nextInt();dinh=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
      }
      bfs(dau);
      if(chuaxet[dinh]){
        System.out.print(-1);
      }
      else{
        ArrayList<Integer> kq=new ArrayList<>();
        kq.add(dinh);
        while (truoc[dinh]!=dau){
          kq.add(truoc[dinh]);
          dinh=truoc[dinh];
        }
        System.out.print(dau);
        for (int i = kq.size() - 1; i >= 0; i--) {
          System.out.print(" -> "+kq.get(i));
        }
      }
      System.out.println();
    }
  }

  private static void bfs(int dau) {
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(dau);
    chuaxet[dau]=false;
    while (!queue.isEmpty()){
      int v=queue.poll();
      for(Integer integer: list[v]){
        if(chuaxet[integer]){
          chuaxet[integer]=false;
          queue.add(integer);
          truoc[integer]=v;
        }
      }
    }
  }
}
