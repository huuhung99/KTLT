package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class LietKeDinhTru {
  static  int n,m;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();
      for(int i=1;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      int tplt=0;
      for(int i=1;i<=n;i++){
        if(chuaxet[i]){
          dfs(i);
          tplt++;
        }
      }
      for(int j=1;j<=n;j++){
        for(int i=1;i<=n;i++) chuaxet[i]=true;
        chuaxet[j]=false;
        int dem=0;
        for(int i=1;i<=n;i++){
          if(chuaxet[i]){
            dfs(i);
            dem++;
          }
        }
        if (dem != tplt) {
          System.out.print(j+" ");
        }
      }
      System.out.println();
    }
  }

  private static void dfs(int i) {
    chuaxet[i]=false;
    for(Integer integer:list[i]){
      if(chuaxet[integer]) dfs(integer);
    }
  }
}
