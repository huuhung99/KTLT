package A_LuyenTap_DoThi;

import java.util.ArrayList;
import java.util.Scanner;

public class DiemNutGiaoThongTrongYeu {
  static int n,m;
  static ArrayList<Integer> list[]=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();
      for(int i=0;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
        list[v].add(u);
      }
      ArrayList<Integer> kq=new ArrayList<>();
      for(int i=1;i<=n;i++){
        int dem=0;
        for(int j=1;j<=n;j++) chuaxet[j]=true;
        chuaxet[i]=false;
        for(int j=1;j<=n;j++)
          if(chuaxet[j]){
            bfs(j);
            dem++;
          }
        if(dem>1) kq.add(i);
      }
      System.out.println(kq.size());
      for (Integer integer : kq) {
        System.out.print(integer+" ");
      }
      System.out.println();
    }
  }

  private static void bfs(int j) {
    chuaxet[j]=false;
    for(Integer integer: list[j]){
      if(chuaxet[integer]) bfs(integer);
    }
  }
}
