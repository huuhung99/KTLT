package A_LuyenTap_DoThi;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;

public class QuayLaiDinh1 {
  static int n,m,truoc[]=new int[21];
  static ArrayList<Integer> list[]=new ArrayList[21];
  static boolean chuaxet[]=new boolean[21];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();m=sc.nextInt();
      for(int i=1;i<=n;i++){
        list[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        list[u].add(v);
      }
      Set<Integer> kq=new HashSet<>();
      bfs(1);
      int dich=2;
      while (truoc[dich]!=1){
        kq.add(truoc[dich]);
        dich=truoc[dich];
      }
      kq.add(1);
      for(int i=1;i<=n;i++) chuaxet[i]=true;
      bfs(2);
      dich=1;
      while (truoc[dich]!=2){
        kq.add(truoc[dich]);
        dich=truoc[dich];
      }
      kq.add(2);
      System.out.println(kq.size());
    }
  }

  private static void bfs(int i) {
    Queue<Integer> queue=new ArrayDeque<>();
    queue.add(i);
    chuaxet[i]=false;
    while (!queue.isEmpty()){
      int v= queue.poll();
      for(Integer integer: list[v]){
        if(chuaxet[integer]){
          queue.add(integer);
          truoc[integer]=v;
          chuaxet[integer]=false;
        }
      }
    }
  }

}
