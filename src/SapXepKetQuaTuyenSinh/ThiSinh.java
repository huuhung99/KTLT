package SapXepKetQuaTuyenSinh;

import java.text.DecimalFormat;

public class ThiSinh implements Comparable<ThiSinh>{
    private String ma,ten,trangThai,khuVuc;
    private double toan,ly,hoa,cong,tongDiem;
    private DecimalFormat decimalFormat=new DecimalFormat("#.#");

    public ThiSinh(String ma, String ten, double toan, double ly, double hoa) {
        this.ma = ma;
        this.ten = ten;
        this.toan = toan;
        this.ly = ly;
        this.hoa = hoa;
        khuVuc=ma.substring(0,3);
        switch (khuVuc){
            case "KV1": cong=0.5;break;
            case "KV2": cong=1.0;break;
            default: cong=2.5;break;
        }
        tongDiem=Math.round((toan+ly+hoa+cong)*10.0)/10.0;
        if(tongDiem>=24.0) trangThai="TRUNG TUYEN";
        else trangThai="TRUOT";
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s",ma,ten,decimalFormat.format(cong),decimalFormat.format(tongDiem),trangThai);
    }

    @Override
    public int compareTo(ThiSinh o) {
        if(tongDiem==o.tongDiem){
            return ma.compareTo(o.ma);
        }
        return tongDiem>o.tongDiem?-1:1;
    }
}
