package SapXepSinhVienTheoNganh;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Map<String,String> map=new HashMap<>();
        map.put("CONG NGHE THONG TIN","DCCN");
        map.put("KE TOAN","DCKT");
        map.put("AN TOAN THONG TIN","DCAT");
        map.put("VIEN THONG","DCVT");
        map.put("DIEN TU","DCDT");
        Scanner sc=new Scanner(System.in);
//        sc=new Scanner(new File("C:/Users/Admin/Desktop/SPOJ/src/sinhvien.txt"));
        int t=Integer.parseInt(sc.nextLine());
        List<SinhVien> list=new ArrayList<>();
        for (int i=0;i<t;i++){
            SinhVien sinhVien=new SinhVien(sc.nextLine(), sc.nextLine(), sc.nextLine(),sc.nextLine());
                list.add(sinhVien);
        }
        int k=Integer.parseInt(sc.nextLine());
        for(int i=0;i<k;i++){
            String tmp= sc.nextLine().trim().toUpperCase();
            System.out.format("DANH SACH SINH VIEN NGANH %s:\n",tmp);
            for(SinhVien sinhVien:list){
                if(sinhVien.getMa().contains(map.get(tmp))){
                    if(tmp.equals("CONG NGHE THONG TIN")||tmp.equals("AN TOAN THONG TIN")){
                        if(sinhVien.getLop().charAt(0)!='E')
                            System.out.println(sinhVien);
                    }
                    else System.out.println(sinhVien);
                }
            }
        }
    }
}
