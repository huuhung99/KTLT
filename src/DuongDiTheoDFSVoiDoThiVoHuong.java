import java.util.ArrayList;
import java.util.Scanner;

public class DuongDiTheoDFSVoiDoThiVoHuong {
    static int n,m,dau,dich,truoc[]=new int[1001];
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            dau=sc.nextInt();
            dich=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                truoc[i]=0;
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            dfs(dau);
            if(chuaxet[dich]) System.out.print(-1);
            else{
                ArrayList<Integer> tmp=new ArrayList<>();
                tmp.add(dich);
                while (truoc[dich]!=dau){
                    tmp.add(truoc[dich]);
                    dich=truoc[dich];
                }
                tmp.add(dau);
                for(int i=tmp.size()-1;i>=0;i--)
                    System.out.print(tmp.get(i)+" ");
            }
            System.out.println();
        }
    }

    private static void dfs(int dau) {
        chuaxet[dau]=false;
        for(Integer i:lists[dau]){
            if(chuaxet[i]){
                chuaxet[i]=false;
                truoc[i]=dau;
                dfs(i);
            }
        }
    }
}
