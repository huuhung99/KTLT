import java.util.*;

public class HinhChuNhatLonNhat {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            int n=sc.nextInt();long a[]=new long[n];
            for(int i=0;i<n;i++) a[i]=sc.nextLong();
            xyLy(a,n);
        }
    }

    private static void xyLy(long[] a, int n) {
        int r[]=new int[n],l[]=new int[n],i;
        Stack<Integer> stack=new Stack<>();
        for(i=n-1;i>=0;i--){
            while (!stack.isEmpty()&& a[i]<=a[stack.peek()]) stack.pop();
            if(stack.isEmpty()) r[i]=n;
            else r[i]=stack.peek();
            stack.push(i);
        }
        stack.clear();
        for(i=0;i<n;i++){
            while (!stack.isEmpty()&& a[i]<=a[stack.peek()]) stack.pop();
            if(stack.isEmpty()) l[i]=-1;
            else l[i]=stack.peek();
            stack.push(i);
        }
        long result=0;
        for(i=0;i<n;i++){
            result=Math.max(result,a[i]*(r[i]-l[i]-1));
        }
        System.out.println(result);
    }
}
