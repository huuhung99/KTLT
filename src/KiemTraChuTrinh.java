import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class KiemTraChuTrinh {
    static int n,m,truoc[]=new int[1001];
    static ArrayList<Integer>[] lists=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001],check=false;

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            for(int i=0;i<=n;i++){
                lists[i]=new ArrayList<>();
                chuaxet[i]=true;
                truoc[i]=0;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v= sc.nextInt();
                lists[u].add(v);
                lists[v].add(u);
            }
            for(int i=1;i<=n;i++){
                if(chuaxet[i]){
                    dfs(i);
                    if(check) break;
                }
            }
            if(check) System.out.println("YES");
            else System.out.println("NO");
            check=false;
        }
    }

    private static void dfs(int i) {
        Queue<Integer> queue=new ArrayDeque<>();
        queue.add(i);
        while (!queue.isEmpty()){
            int v=queue.poll();
            chuaxet[v]=false;
            for(Integer integer:lists[v]){
                if(chuaxet[integer]){
                    chuaxet[integer]=false;
                    queue.add(integer);
                    truoc[integer]=v;
                }
                else if(integer!=truoc[v]){
                    check=true;
                    queue.clear();
                    break;
                }
            }
        }
    }
}
