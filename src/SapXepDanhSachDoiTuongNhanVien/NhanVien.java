package SapXepDanhSachDoiTuongNhanVien;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NhanVien implements Comparable<NhanVien>{
    private String maNhanVien,ten,gioiTinh,diaChi,maSoThue,ngayKiHopDong;
    private Date ngaySinh;

    public NhanVien(int maNhanVien, String ten, String gioiTinh, String ngaySinh, String diaChi, String maSoThue, String ngayKiHopDong) throws ParseException {
        this.maNhanVien = String.format("%05d",maNhanVien);
        this.ten = ten;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        this.diaChi = diaChi;
        this.maSoThue = maSoThue;
        this.ngayKiHopDong = ngayKiHopDong;
    }
    public String toString(){
        return String.format("%s %s %s %s %s %s %s",maNhanVien,ten,gioiTinh,new SimpleDateFormat("dd/MM/yyyy").format(ngaySinh),diaChi,maSoThue,ngayKiHopDong);
    }
    @Override
    public int compareTo(NhanVien nhanVien){
        return ngaySinh.compareTo(nhanVien.ngaySinh);
    }
}
