package SapXepDanhSachDoiTuongNhanVien;

import java.text.ParseException;
import java.util.*;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<NhanVien> list=new ArrayList<>();
        for(int i=1;i<=t;i++){
            NhanVien nhanVien=new NhanVien(i, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            list.add(nhanVien);
        }
        Collections.sort(list);
        for(NhanVien nhanVien:list)
            System.out.println(nhanVien);
    }
}
