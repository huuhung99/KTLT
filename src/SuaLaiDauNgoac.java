import java.util.Scanner;
import java.util.Stack;

public class SuaLaiDauNgoac {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        Stack<Character> stack=new Stack<>();
        int t=Integer.valueOf(sc.nextLine());
        while(t-->0){
            int count=0;
            String s=sc.nextLine();
            for(int i=0;i<s.length();i++){
                if(s.charAt(i)=='(')
                    stack.push('(');
                else if(s.charAt(i)==')'&& stack.isEmpty()){
                    stack.push('(');
                    count++;
                }
                else{
                    stack.pop();
                }
            }
            System.out.println(count+stack.size()/2);
            stack.clear();
        }
    }
}
