import java.math.BigInteger;
import java.util.Scanner;

public class ChiaHet {

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      BigInteger so1=new BigInteger(sc.next());
      BigInteger so2=new BigInteger(sc.next());
      if (so1.mod(so2).equals(new BigInteger("0")) || so2.mod(so1).equals(new BigInteger("0"))) {
        System.out.println("YES");
      } else {
        System.out.println("NO");
      }
    }
  }
}
