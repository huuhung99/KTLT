package LopINTSET;

import java.util.TreeSet;

public class IntSet {
    private TreeSet<Integer> set=new TreeSet<>();
    public IntSet(int a[]){
        for(int i=0;i<a.length;i++){
            set.add(a[i]);
        }
    }

    public IntSet() {
    }

    @Override
    public String toString() {
        String tmp="";
        for(Integer i:set)
            tmp+= String.valueOf(i)+ " ";
        return tmp;
    }

    public IntSet union(IntSet s2) {
        IntSet intSet=new IntSet();
        intSet.set.addAll(set);
        intSet.set.addAll(s2.set);
        return  intSet;
    }
}
