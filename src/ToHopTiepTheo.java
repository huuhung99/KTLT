import java.util.Scanner;

public class ToHopTiepTheo {
    static int c[]=new int[21],a[],n,k,count;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            n= sc.nextInt();k=sc.nextInt();
            count=0;
            a=new int[k];
            for(int i=0;i<k;i++)a[i]=sc.nextInt();
            quaylui(1,false,false);
            System.out.println(count);
        }
    }

    private static void quaylui(int i,boolean check,boolean check2) {
        if(check==false&&check2==false)
            for(int j=c[i-1]+1;j<=n-k+i;j++){
                c[i]=j;
                if(i==k){
                    if(xuly()){
                        quaylui(i+1,true,false);
                    }
                }
                else quaylui(i+1,false,false);
            }
        else if(check==true&&check2==true){
            for(int j=1;j<=n;j++){
                if(c[j]!=a[j-1]){
                    count++;
                }
            }
        }
        else {
            for(int j=c[i-1]+1;j<n-k+i;j++){
                c[i]=j;
                if(i==n){
                    if(xuly()){
                        quaylui(i+1,true,true);
                    }
                }
                else quaylui(i+1,true,false);
            }
        }
    }

    private static boolean xuly() {
        for(int i=1;i<=k;i++){
            if(c[i]!=a[i-1]){
                return false;
            }
        }
        return true;
    }
}
