package HDT;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class GhepMa {

  public static void main(String[] args) throws Exception {
    List<String> data1 = (List<String>) readObjectFromFile(
        "DATA1.in");
    List<Integer> data2 = (List<Integer>) readObjectFromFile(
        "DATA2.in");
    TreeSet<String> set = new TreeSet();
    for (int i = 0; i < data1.size(); i++) {
      set.add(fiveFirstChar(data1.get(i)) + threeFinalNumber(data2.get(i)));
    }
    Iterator<String> iterator = set.iterator();
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }
  }

  private static String fiveFirstChar(String s) {
    return s.substring(0, 5);
  }

  private static String threeFinalNumber(Integer integer) {
    return String.format("%03d", integer % 1000);
  }

  private static Object readObjectFromFile(String filepath) throws Exception {
    FileInputStream fileIn = new FileInputStream(filepath);
    ObjectInputStream objectIn = new ObjectInputStream(fileIn);

    Object obj = objectIn.readObject();

    objectIn.close();
    return obj;
  }

}
