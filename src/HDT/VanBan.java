package HDT;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class VanBan {

  public static void main(String[] args) throws Exception {
    Scanner sc=new Scanner(new File("DANHSACH.in"));
    List<String> kq = new ArrayList<>();
    while (sc.hasNext()){
      kq.add(chuanHoa(sc.nextLine()));
    }
    Collections.sort(kq, (s1, s2) -> {
      String[] ss1 = s1.split(" ");
      String[] ss2 = s2.split(" ");
      if(ss1[ss1.length-1].equals(ss2[ss2.length-1])){
        if(ss1[0].equals(ss2[0])){
          return ss1[1].compareTo(ss2[1]);
        }
        else return ss1[0].compareTo(ss2[0]);
      }
      else return ss1[ss1.length-1].compareTo(ss2[ss2.length-1]);
    });
    for(String i : kq){
      System.out.println(i);
    }
  }

  private static String chuanHoa(String nextLine) {
    String[] s= nextLine.trim().toLowerCase().split("\\s+");
    String kq="";
    for(int i=0;i<s.length;i++){
      s[i] = Character.valueOf((char)(s[i].charAt(0)-32))+s[i].substring(1,s[i].length());
      kq+=s[i] +" ";
    }
    return kq;
  }
}
