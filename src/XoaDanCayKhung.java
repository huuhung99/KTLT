import java.util.ArrayList;
import java.util.Scanner;

public class XoaDanCayKhung {
  static int n,buoc;
  static ArrayList<Integer>[] lists=new ArrayList[100001];
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      buoc=sc.nextInt();
      for(int i=0;i<=n;i++){
        lists[i]=new ArrayList<>();
      }
      for(int i=1;i<n;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        lists[u].add(v);
        lists[v].add(u);
      }
      boolean check=true;
      for(int i=1;i<=buoc;i++){
        for(int j=1;j<=n;j++){
          if(!lists[j].isEmpty()){
            check=false;
            break;
          }
        }
        if(check) {
          System.out.println(0);
          break;
        }
        check=true;
        for(int j=1;j<=n;j++){
          if(lists[j].size()==1) {
            for(int k=1;k<=n;k++){
              lists[k].remove(Integer.valueOf(j));
            }
            lists[i]=new ArrayList<>();
          }
        }
      }
      if(!check){
        int dem=0;
        for(int i=1;i<=n;i++){
          if(!lists[i].isEmpty()) dem++;
        }
        System.out.println(dem);
      }
    }
  }
}
