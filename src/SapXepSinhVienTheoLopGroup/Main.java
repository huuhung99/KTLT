package SapXepSinhVienTheoLopGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<SinhVien> list=new ArrayList<>();
        for (int i=0;i<t;i++){
            SinhVien sinhVien=new SinhVien(sc.nextLine(), sc.nextLine(), sc.nextLine(),sc.nextLine());
//            SinhVien sinhVien=new SinhVien("", sc.nextLine(), sc.nextLine(),"");
            list.add(sinhVien);
        }
        int k=Integer.parseInt(sc.nextLine());
        for(int i=0;i<k;i++){
            String tmp= sc.nextLine();
            System.out.format("DANH SACH SINH VIEN LOP %s:\n",tmp);
            for(int j=0;j<t;j++){
                if(list.get(j).getLop().equals(tmp))
                    System.out.println(list.get(j));
            }
        }
    }
}
