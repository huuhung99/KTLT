import java.util.Scanner;

public class ChuanHoaXauHoTen {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        long t= Long.valueOf(sc.nextLine());
        while(t-->0){
            String[] s = sc.nextLine().trim().toLowerCase().split("\\W+");
            for(int i=0;i<s.length;i++){
                if(s[i].isEmpty()||s[i].equals("")) continue;
                else{
                    char[] chars = s[i].toCharArray();
                    chars[0]-=32;
                    for(int j=0;j<chars.length;j++)
                        System.out.print(chars[j]);
                    if(i!=s.length-1)System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
    private static long gcd(long a, long b)
    {
        if (a == 0)
            return b;
        return gcd(b % a, a);
    }
    private static long lcm(long a, long b)
    {
        return (a / gcd(a, b)) * b;
    }
    private static boolean isPrime(long n) {
        if(n<2) return false;
        if(n<4) return true;
        if(n%2==0||n%3==0)
            return false;
        for(long i=5;i*i<=n;i+=6)
            if(n%i==0||n%(i+2)==0)
                return false;
        return true;
    }
}
