import java.util.Scanner;
import java.util.Stack;

public class PhanTuDauTienBenPhaiLonHon {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            int n=sc.nextInt(),a[]=new int[n];
            for(int i=0;i<n;i++) a[i]=sc.nextInt();
            xuly(a,n);
        }
    }

    private static void xuly(int[] a, int n) {
        int r[]=new int[n],i;
        Stack<Integer> stack=new Stack<>();
        for(i=n-1;i>=0;i--){
            while (!stack.isEmpty()&& a[i]>=stack.peek()) stack.pop();
            if(stack.isEmpty()) r[i]=-1;
            else r[i]=stack.peek();
            stack.push(a[i]);
        }
        for(i=0;i<n;i++){
            System.out.print(r[i]+" ");
        }
        System.out.println();
    }
}
