package BaiToanTinhCong;

public class NhanVien {
    private String ma,ten,chucVu;
    private long luongCoBan,ngayCong,luongThang,thuong,phuCap,thuNhap;

    public NhanVien(int ma, String ten, long luongCoBan, long ngayCong, String chucVu) {
        this.ma = "NV"+String.format("%02d",ma);
        this.ten = ten;
        this.chucVu = chucVu;
        this.luongCoBan = luongCoBan;
        this.ngayCong = ngayCong;
        this.luongThang=ngayCong*luongCoBan;
        switch (chucVu){
            case "GD": this.phuCap=250000; break;
            case "PGD":this.phuCap=200000; break;
            case "TP":this.phuCap=180000; break;
            default:this.phuCap=150000; break;
        }
        if(ngayCong<22) this.thuong=0;
        else if(ngayCong>24) this.thuong=this.luongThang/5;
        else this.thuong=this.luongThang/10;
        this.thuNhap=this.luongThang+this.thuong+this.phuCap;
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %d %d %d",ma,ten,luongThang,thuong,phuCap,thuNhap);
    }
}
