import java.util.*;

public class Solution {
    public static void main(String[] args) throws Exception{
        Map<Long,Long> map=new HashMap<>();
        for(long i=1;i<1000001;i++){
            map.put(i*i*i,i);
        }
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            long l = sc.nextLong();
            if(map.containsKey(l)){
                System.out.println(map.get(l));
            }
            else System.out.println(-1);
        }
    }

}
