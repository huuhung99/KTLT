import java.util.Scanner;
import java.util.Stack;

public class DanhSoThuTuCapDauNgoac {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        while (t-->0){
            String s=sc.nextLine();
            Stack<Integer> stack=new Stack<>();
            Stack<Integer> stack2=new Stack<>();
            int count=0;
            for(int i=0;i<s.length();i++){
                if(s.charAt(i)=='('){
                    count++;
                    stack.push(count);
                    stack2.push(count);
                }
                else if(s.charAt(i)==')'){
                    stack2.push(stack.pop());
                }
            }
            for(Object o:stack2.stream().toArray())
                System.out.print(o +" ");
            System.out.println();
        }
    }
}
