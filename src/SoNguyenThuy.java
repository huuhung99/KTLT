import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

public class SoNguyenThuy {
  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    ArrayList<String> list=new ArrayList<>();
    Queue<String> queue=new ArrayDeque<>();
    queue.add("44");
    queue.add("55");
    list.add("44");
    list.add("55");
    int count=0;
    while (count<10005){
      ArrayList<String> tmp=new ArrayList<>(queue);
      queue.clear();
      for(int i=0;i<tmp.size();i++){
        String s = "4" + tmp.get(i) + "4";
        queue.add(s);
        list.add(s);
      }
      for(int i=0;i<tmp.size();i++){
        String s="5"+tmp.get(i)+"5";
        queue.add(s);
        list.add(s);
      }
      count+=tmp.size();
    }
    while(t-->0){
      int n=sc.nextInt();
      for (int i = 0; i < n; i++) System.out.print(list.get(i)+" ");
      System.out.println();
    }
  }
}
