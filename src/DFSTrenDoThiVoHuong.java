import java.util.ArrayList;
import java.util.Scanner;

public class DFSTrenDoThiVoHuong {
    static int n,m;
    static ArrayList<Integer>[] list=new ArrayList[1001];
    static boolean chuaxet[]=new boolean[1001];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t= sc.nextInt();
        while(t-->0){
            n=sc.nextInt();
            m=sc.nextInt();
            int s=sc.nextInt();
            for(int i=0;i<=n;i++){
                list[i]=new ArrayList<>();
                chuaxet[i]=true;
            }
            for(int i=1;i<=m;i++){
                int u=sc.nextInt();
                int v=sc.nextInt();
                list[u].add(v);
                list[v].add(u);
            }
            dfs(s);
            System.out.println();
        }
    }

    private static void dfs(int u) {
        System.out.print(u+" ");
        chuaxet[u]=false;
        for(Integer v:list[u]){
            if(chuaxet[v]) dfs(v);
        }
    }
}
