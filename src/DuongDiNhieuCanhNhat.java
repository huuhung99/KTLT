import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class DuongDiNhieuCanhNhat {
  static int n,m,dau,dich,truoc[]=new int[1001];
  static ArrayList<Integer>[] lists=new ArrayList[1001];
  static boolean chuaxet[]=new boolean[1001];

  public static void main(String[] args) {
    Scanner sc=new Scanner(System.in);
    int t=sc.nextInt();
    while (t-->0){
      n=sc.nextInt();
      m=sc.nextInt();
      dau=sc.nextInt();
      dich=sc.nextInt();
      for(int i=0;i<=n;i++){
        lists[i]=new ArrayList<>();
        chuaxet[i]=true;
        truoc[i]=0;
      }
      for(int i=1;i<=m;i++){
        int u=sc.nextInt();
        int v=sc.nextInt();
        lists[u].add(v);
        lists[v].add(u);
      }

    }
  }
  private static void bfs(int u) {
    Queue<Integer> queue=new LinkedList<>();
    queue.add(u);chuaxet[u]=false;
    while (!queue.isEmpty()){
      int v= queue.poll();
      for(Integer i:lists[v]){
        if(chuaxet[i]){
          queue.add(i);
          truoc[i]=v;
          chuaxet[i]=false;
        }
      }
    }
  }
}
