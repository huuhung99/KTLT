package TinhGio;

public class Gio implements Comparable<Gio>{
    private int gio;
    private int phut;

    public Gio(int gio, int phut) {
        this.gio = gio;
        this.phut = phut;
    }

    @Override
    public String toString(){
        return String.format("%d gio %d phut",gio,phut);
    }

    public Gio tru(Gio vao) {
        int tmpPhut=0;
        int tmpGio=0;
        if(phut< vao.phut){
            tmpPhut=60;
            tmpGio=-1;
        }
        tmpPhut+=phut- vao.phut;
        tmpGio+=gio- vao.gio;
        if(tmpGio<=0) tmpGio+=+24;
        return new Gio(tmpGio,tmpPhut);
    }

    @Override
    public int compareTo(Gio o) {
        if(gio==o.gio){
            return phut>o.phut?-1:1;
        }
        return gio>o.gio?-1:1;
    }
}
