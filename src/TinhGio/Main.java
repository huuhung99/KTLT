package TinhGio;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=Integer.parseInt(sc.nextLine());
        List<NguoiChoi> list=new ArrayList<>();
        for(int i=0;i<t;i++){
            NguoiChoi nguoiChoi=new NguoiChoi(sc.nextLine(), sc.nextLine(), sc.nextLine(),sc.nextLine());
            list.add(nguoiChoi);
        }
        Collections.sort(list);
        for(NguoiChoi nguoiChoi:list)
            System.out.println(nguoiChoi);
    }
}
