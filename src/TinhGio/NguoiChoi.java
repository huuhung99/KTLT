package TinhGio;

public class NguoiChoi implements Comparable<NguoiChoi>{
    private String maNguoiChoi,ten;
    private Gio vao,ra;

    public NguoiChoi(String maNguoiChoi, String ten, String vao, String ra) {
        this.maNguoiChoi = maNguoiChoi;
        this.ten = ten.trim();
        this.vao = stringToGio(vao.trim());
        this.ra = stringToGio(ra.trim());
    }
    private Gio stringToGio(String s){
        String[] split = s.split(":");
        return new Gio(Integer.parseInt(split[0]),Integer.parseInt(split[1]));
    }

    @Override
    public int compareTo(NguoiChoi o) {
        return ra.tru(vao).compareTo(o.ra.tru(vao));
    }

    @Override
    public String toString() {
        return String.format("%s %s %s",maNguoiChoi,ten,ra.tru(vao));
    }
}
