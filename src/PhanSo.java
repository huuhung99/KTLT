import java.util.Scanner;

public class PhanSo {
    private static class PHANSO{
        public long tu;
        public long mau;
        PHANSO(long tu,long mau){
            this.tu=tu;
            this.mau=mau;
        }
        public void rutNgon(){
            long ucln=gcd(this.tu,this.mau);
            this.tu/=ucln;
            this.mau/=ucln;
        }
        long gcd(long n1, long n2) {
            if (n2 == 0) {
                return n1;
            }
            return gcd(n2, n1 % n2);
        }

        public void in() {
            System.out.format("%d/%d",this.tu,this.mau);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PHANSO phanso=new PHANSO(sc.nextLong(),sc.nextLong());
        phanso.rutNgon();
        phanso.in();
    }
}
