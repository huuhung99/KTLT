import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Scanner;

public class BienDoiST2 {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int n=sc.nextInt();
        while (n-->0){
            int s= sc.nextInt(); int t=sc.nextInt();
            if(s>=t){
                System.out.println(s-t);
                continue;
            }
            else {
                Queue<Integer> queue=new ArrayDeque<>();
                int count=0,tmp=1000000000;
                boolean check=true;
                queue.add(s);
                while (!queue.isEmpty()){
                    Integer poll = queue.poll();
                    if(poll==t){
                        System.out.println(Math.min(count,tmp));
                        check=false;
                        break;
                    }
                    count++;
                    int tru=poll-1;
                    int nhan=poll*2;
                    if(tru>0&&nhan<10000)queue.add(tru);
                    if(nhan>=t){
                        tmp=Math.min(tmp,count+nhan-t);
                    }
                    else {
                        if(nhan>0&&nhan<10000)queue.add(nhan);
                    }
                }
                if(check==true) System.out.println(Math.min(count,tmp));
            }
        }
    }
}
