import java.util.Scanner;

public class NhaKhongLienKe {
    static int a[],f[];
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while (t-->0){
            int n=sc.nextInt();
            a=new int[n];
            f=new int[n];
            for(int i=0;i<n;i++) a[i]=sc.nextInt();
            f[0]=a[0];
            f[1]=Math.max(a[0],a[1]);
            for(int i=2;i<n;i++){
                f[i]=Math.max(f[i-2]+a[i],f[i-1]);
            }
            System.out.println(f[n-1]);
        }
    }
}
