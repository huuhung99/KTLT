import java.util.*;

public class So0VaSo9 {
    public static void main(String[] args) throws Exception{
        Scanner sc=new Scanner(System.in);
        int t=sc.nextInt();
        while(t-->0){
            Long n=sc.nextLong();
            Queue<Long> queue=new ArrayDeque<>();
            queue.add(9l);
            while(!queue.isEmpty()){
                Long remove = queue.remove();
                if(remove%n!=0){
                    queue.add(remove*10);
                    queue.add(remove*10+9);
                }
                else {
                    System.out.println(remove);
                    break;
                }
            }
        }
    }
}
